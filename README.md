# README #

# 5.3.4 Kalkulator življenskih stroškov #

## Opis naloge ##

V okviru predmeta spletno programiranje smo si zadali nalogo Kalkulator življenskih stroškov. Vsi poznate situacijo. 
Nekje sredi meseca je denarnica prazna, niste pa točno prepričani kam je izginil vaš denar. 
Stanje na vašem računu ravno tako ni rožnato, kljub pa bi radi zimske počitnice izkoristili za oddih v toplih krajih. 
Guruji varčevanja, avtorji knjig kako postati miljonar in na sploh tisti, ki mislijo, da je obrestno-obrestni račun magično sredstvo, 
ki omogoča iz nič ustvarjati miljone, se prav vsi strinjajo, da je začetek vsakega varčevanje vsekakor nadzor naših izdatkov.

Napisali bomo spletno aplikacijo, ki bo uporabnikom omogočala vodenje stanja različnih računov (npr. transakcijski račun, varčevalni račun, denarnica),
vnašanje izdatkov, katergoriziranje izdatkov (npr. izdatki za avto, študij, zabavo, hrano) in pripravo različnih poročil (npr. pregled izdatkov po kategorijah, spreminjanje izdatkov po mesecih, ...). 
Naša aplikacija bo omogočala nastavljanje ciljev (npr. privarčevati vsaj 100EUR mesečno ali privarčevati 1000EUR do junija 2012) in seveda spremljanje izpolnjevanje teh ciljev.
Zgledujemo se po programih GNUCash, Quicken, Toshl,...

# Zaslonske maske #

## frontpage ##

Zaslonska maska frontpage prikazuje osnovno stran na katero bo uporabnik najprej prišel kadar je prijavljen na svoj račun. Razdeljeno mamo na prikaz na mobilni napravi in prikaz na osebnem računalniku.

** Mobilna naprava **

Po prijavi vas bo aplikacija preusmerila na prvi frontpage kjer je na prikazu graf trenutnega stanja, stroškov in cilj varčevanja za trenutni mesec. Pod grafom je hkrati tudi prikaz mesečnih enkratnih stroškov
za lažjo dodajanje enkratnih stroškov (npr. smo na kavi in želimo dodati kavo kot strošek in lahko enostavno pritisnimo gumb +1). Hkrati pa lahko dostopamo do ostalih pregledov/funkcionalnosti aplikacije preko gumbov (npr. pregled mesečnih stroškov).
Nove Stroške in prilive lahko ravno tako dodamo preko gumbov. Spodnja slika prikazuje frontpage na mobilni napravi.


![Alt text](https://image.prntscr.com/image/IUEGq9e6SVudqhKAlxOJYw.png)
![Alt text](https://image.prntscr.com/image/PE9fupFsQniVaNmFESoqQw.png)

** Osebni računalnik ** 

Po prijavi vas bo aplikacija preusmerila na prvi frontpage, ki je razdeljen na dva dela. Levi del ima tako kot pri mobilni napravi graf trenutnega stanja za ta mesec. Pod grafom imamo pregled vseh stroškov za ta mesec kjer jih lahko urejamo,
posodabljamo in dodajamo. Če želimo dodati strošek ali priliv imamo gumb, ki nam odpre formo na kateri imamo na voljo kategorijo/ime, količino, za kateri račun naj se to izvede in ali je mesečni ali statičen/enkraten
strošek. Na desni strani imamo prikazane trenutne račune ter njihova stanja. Pod računi imamo gumb, ki nam odpre formo za dodajanje računa. Na vrhu imamo tudi gumb, ki nas lahko odnese do statističnih podatkov.
Spodnja slika prikazuje frontpage na osebnem računalniku.

![Alt text](https://image.prntscr.com/image/EclsTHChQ9SXVVEXGg9U_Q.png)

## Pozabljeno geslo ##

Zaslonska maska za pozabljeno geslo prikazuje način kako lahko uporabnik zamenja ali posodobi svoje geslo. Spodnji dve sliki sta prikaz te funkcionalnosti na mobilni napravi in osebnem računalniku.

![Alt text](https://image.prntscr.com/image/l69hLRG2TfSXVe3Wvap7-Q.png)

## Prijava ##

Zaslonska maska prijava je primer začetne strani na katero bo uporabnik prišel, ko bo zaželel uporabiti našo aplikacijo. Na voljo je prijava če že ima račun, pozabljeno geslo, če je geslo pozabil in ga
bo preusmerilo na stran kjer lahko zamenja geslo ter registracija, ki ga bo preusmerilo na stran kjer lahko kreira svoj račun. Spodnja slika na pokaže kako bo izgladala prijava na mobilni napravi in osebnem računalniku.

![Alt text](https://image.prntscr.com/image/GBZ6RlT3RNKMNW8U15AbaA.png)

## Registracija ##

Zaslonska maska prikazuje registracijo novega uporabniškega računa na mobilni napravi in osebnem računalniku.

![Alt text](https://image.prntscr.com/image/ilYg8C3zSvOD9Hcekh0rmA.png)

## Statistika in varčevanje ##

Prikaz funkcionalnosti bo drugačen na mobilnih napravah kakor na osebnem računalniku.

** Mobilna naprava **

Na mobilni napravi bo uporabnik imel gumb s katerim bo lahko dostopal do strani varčevanje. Na strani bo lahko dodal in spremljal potek varčevalnih ciljev, hkrati bo imel tudi gumb s katerim bo lahko dostopal do
strani, kjer bo prikazala poročila o računih.

![Alt text](https://image.prntscr.com/image/CTjpC2DrQNCxGFyypguhVg.png)

** Osebni računalnik **

Na osebnem računalniku bo uporabnik imel na pregled poročila in grafe o svojih računih. Hkrati pa bo imel na prikazu potek varčevalnih ciljev in opcijo dodajanja varčevalnih ciljev. Z razliko od mobilne naprave
bodo statistični in varčevalni cilji prikazani na eni strani.

![Alt text](https://image.prntscr.com/image/EzHL9Lt0TCmIERZONWSmqg.png)


# Pregled povezav #

Spodnja slika prikazuje kako bodo strani povezane med seboj za mobilno napravo in osebni računalnik ter kaj bo uporabniku v določeni točki omogočeno.

** Mobilna naprava **

![Alt text](https://image.prntscr.com/image/Dyptq8nUTGyRabnCzed34g.png)

** Osebni računalnik **

![Alt text](https://image.prntscr.com/image/oX06xHfuTWiFuKzqnpgMJw.png)

# Preverjanje izgleda #

** 27. novembra leta 2017 gospodovega **

Po preverjanju na naslednjih brskalnikih: opera,chrome,edge(za windows mobile),chrome(za android), mozzila firefox smo prišli do ugotovitve,
da je spletna stran v večini sodobnih brskalnikov enaka, in da ne pride do večjih sprememb. 

** Glavna stran/Frontpage **

Preden dobimo dostop do glavne strani, se je potrebno registrirati(registracija.jade) ali pa prijaviti(prijava.jade) z uporabniškim računom. Po prijavi se nam prikaže
glavna stran(pod imenom index.jade).

# Dinamična spletna aplikacija s podatkovno bazo 18.12.2017 #

**koraki za vzpostavitev**

1.git clone https://bitbucket.org/mrWhoop/sptoshl

2.cd sptoshl

3.cd project

4.npm install -g

5.npm install -g nodemon

6.sudo apt-get remove mongodb-org mongodb-org-server

7.sudo apt-get autoremove

8.sudo rm -rf /usr/bin/mongo*

9.echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

10.sudo apt-get update

11.sudo apt-get install mongodb-org mongodb-org-server

12.sudo touch /etc/init.d/mongod

13.sudo apt-get install mongodb-org-server

14.npm install --save mongoose

15.npm install --save request

16.cd ..

17.cd mongodb

18.mkdir data

19.echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod

20.chmod a+x mongod

**za zagon baze**

1.gremo pod /mongodb

2../mongod

**za zagon aplikacije(baza mora laufat na drugi konzoli)**

1.gremo pod /project

2.nodemon

**Heroku povezava**

https://afternoon-ridge-89903.herokuapp.com/

# Angular in testiranje aplikacije #

** JMeter **

Priložena slika prikazuje JMeter simulacijo nad našo aplikacijo in rezultate.

![alt text](https://image.prntscr.com/image/98AF_-gYQiSWHQs-eNnMGQ.png)

** Zap **

Zap smo zagnali na naši SPA aplikaciji in pridobili naslednje rezultate. Testirali smo na dveh brskalnik Chrome in Opera,

Chrome je prikazal naslednje rezultate:

-število zahtev: 51

-preneseno: 1,4 MB

-Finish: 4,66s 

-DOMContetLoaded: 1,76s 

-Load: 1,76 

Opera pa naslednje:

-število zahtev: 51

-preneseno: 1,4 MB

-Finish: 4,42s 

-DOMContetLoaded: 1,52 

-Load: 1,52

** Ranljivosti ** 

Z zapom smo tudi našli ranljivosti, ki so prikazane na naslednjih dveh slikah

![alt text](https://image.prntscr.com/image/DcGSlOe5TxGv7TcfXbrS1A.png)


![alt text](https://image.prntscr.com/image/M_eFlEtHQySx0TSSbYfr4g.png)


kot zanimivost prilagamo sliko poiskusa napada, kjer je probal z parametri v url-ju dostopat do nekaterih podatkov, vendar
kakor slika prikazuje se mu je vrnilo le tisto kar mu je dovoljeno.

![alt text](https://image.prntscr.com/image/n6MqjqBpScCLPf3pheTh7Q.png)

** Katalon ** 

S katalonom smo testirali in simulirali pritiske na gumbe in pridobili naslednje rezultate. 

![alt text](https://image.prntscr.com/image/VyiQNPsoTV_2OEEvad4y0A.png)


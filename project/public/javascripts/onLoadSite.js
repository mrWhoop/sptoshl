window.onload = function welcome(){
  var trenutnaLokacija = window.location.href;
  var razdeli = trenutnaLokacija.split("/");
  console.log(razdeli);
  localStorage.setItem("idUporabnika",razdeli[4]);
  console.log(localStorage.getItem("idUporabnika"));
  progressBarValues();
}
//funkcija za updatanje progress bar tuki ne bomo brali objektov ampak bomo
//dejansko brali iz tabele
//za zdej je na objekte vezano
function progressBarValues(){
  
  
  var progressMesecni = document.getElementById("barMesecniStroski");
  var progressEnkratni = document.getElementById("barEnkratniStroski");
  var progressStanje = document.getElementById("barStanje");
  var progressVarcevanje = document.getElementById("barVarcevanje");
  
  var vsotaRacunov = 0;
  var vsotaMesecnihStroskov = 0;
  var vsotaEnkratnihStroskov = 0;
  var vsotaVarcnihCiljev = 0;
  
  var trenutniRacuni = document.getElementById("bodyOfRacuni").childNodes;
  if(progressMesecni != null){
    
    var trenutniStroskiInPriljivi = document.getElementById("bodyOfStroski").childNodes;
    
    console.log(trenutniStroskiInPriljivi);
    var stroski;
    for (stroski in trenutniStroskiInPriljivi){
        if(stroski == 'length'){
          
          break;
        }
        console.log(trenutniStroskiInPriljivi[stroski].childNodes[2].innerHTML);
        if(trenutniStroskiInPriljivi[stroski].childNodes[1].innerHTML == 'Strošek'){
          console.log("hello");
          if(trenutniStroskiInPriljivi[stroski].childNodes[4].firstChild.value != null){
              console.log(vsotaEnkratnihStroskov);
              vsotaEnkratnihStroskov += Number(trenutniStroskiInPriljivi[stroski].childNodes[3].innerHTML);
          }else{
              console.log(vsotaMesecnihStroskov);
              vsotaMesecnihStroskov += Number(trenutniStroskiInPriljivi[stroski].childNodes[3].innerHTML);
          }
        }else{
          vsotaRacunov += Number(trenutniStroskiInPriljivi[stroski].childNodes[3].innerHTML);
        }
        
    }
    var cilji = document.getElementById("cilji").childNodes;
    console.log(cilji.length);
    var i = 0;
    for(i = 0;i < cilji.length; i++){
      console.log(cilji[i]);
      vsotaVarcnihCiljev += Number(cilji[i].getAttribute("value"));
    }
    console.log(vsotaVarcnihCiljev);
    if(vsotaRacunov == 0){
      vsotaVarcnihCiljev = 0;
    }else{
      vsotaRacunov = vsotaRacunov-vsotaEnkratnihStroskov-vsotaMesecnihStroskov-vsotaVarcnihCiljev;
    }
    if(vsotaRacunov < 0){
      vsotaVarcnihCiljev = vsotaVarcnihCiljev+vsotaRacunov;
      vsotaRacunov = 0;
    }
    var skupajVsota = (vsotaRacunov)+vsotaEnkratnihStroskov+vsotaMesecnihStroskov+vsotaVarcnihCiljev;
    //TODO trenutno se ne racunamo varcnihCiljev
    var procentMesecnihStroskov = (vsotaMesecnihStroskov/skupajVsota)*100;
    var procentEnkratnihStroskov = (vsotaEnkratnihStroskov/skupajVsota)*100;
    var procentVarcnihCiljev = (vsotaVarcnihCiljev/skupajVsota)*100;
    
    var procentKoncnoStanje = (vsotaRacunov/skupajVsota)*100;
    
    //dodajanje vrednosti progressBarValues
    
    
    progressMesecni.title = ("Mesečni stroški: "+vsotaMesecnihStroskov.toFixed(2)+'€').toString();
    progressEnkratni.title = ("Enkratni stroški: "+vsotaEnkratnihStroskov.toFixed(2)+'€').toString();
    progressStanje.title = ("Trenutno stanje(vseh računov): "+vsotaRacunov.toFixed(2)+'€').toString();
    progressVarcevanje.title = ("Varčevalni cilji: "+vsotaVarcnihCiljev.toFixed(2)+'€').toString();
    
    progressMesecni.style.width = procentMesecnihStroskov.toFixed(2)+'%';
    progressEnkratni.style.width = procentEnkratnihStroskov.toFixed(2)+'%';
    progressStanje.style.width = procentKoncnoStanje.toFixed(2)+'%';
    progressVarcevanje.style.width = procentVarcnihCiljev.toFixed(2)+'%';
  
  }
}
function brisi(id){
      console.log(id);
      
      if(confirm('ali ste prepričani, da želite brisati?')){
        var element = document.getElementById(id).parentElement;
        console.log(element);
        console.log(element.parentNode);
        parent = element.parentNode;
        parent.parentNode.removeChild(parent);
        progressBarValues();
      }
    }
function brisiEnkratni(id){
  id = id.substring(0,id.length-1);
  if(confirm('ali ste prepričani, da želite brisati?')){
    var element = document.getElementById(id);
    var tip;
    console.log(element);
    var idUporabnika = localStorage.getItem("idUporabnika");
    if(element.childNodes[1].innerHTML == 'Strošek'){
      var pot = '/deleteEnkratni/'+idUporabnika+'/strosek/'+element.id+'/mesec/'+localStorage.getItem("mesec");
      var form = document.createElement("form");
        form.setAttribute("method","get");
        form.setAttribute("action",pot);
      
        document.body.appendChild(form);
        form.submit();  
      
      
    }else{
      var pot = '/deleteEnkratni/'+idUporabnika+'/priliv/'+element.id+'/mesec/'+localStorage.getItem("mesec");
      var form = document.createElement("form");
        form.setAttribute("method","get");
        form.setAttribute("action",pot);
      
        document.body.appendChild(form);
        form.submit();  
      
      
    }
  }
}
function brisiMesecni(id){
    var element = document.getElementById(id).parentElement.parentElement;
    
  if(element.childNodes[4].innerHTML != "Nepotrjeno"){
    if(confirm('ali ste prepričani, da želite brisati?')){
      element = document.getElementById(id).parentElement.parentElement;
      var tip;
      var idUporabnika = localStorage.getItem("idUporabnika");
      
      if(element.childNodes[1].innerHTML == 'Strošek'){
        var pot = '/deleteMesecni/'+idUporabnika+'/strosek/'+element.id+'/mesec/'+localStorage.getItem("mesec");
        var form = document.createElement("form");
          form.setAttribute("method","get");
          form.setAttribute("action",pot);
        
          document.body.appendChild(form);
          form.submit();  
      }else{
        
        var pot = '/deleteMesecni/'+idUporabnika+'/priliv/'+element.id+'/mesec/'+localStorage.getItem("mesec");
        var form = document.createElement("form");
          form.setAttribute("method","get");
          form.setAttribute("action",pot);
        
          document.body.appendChild(form);
          form.submit();  
        
        
      }
    }
  }else{
    var otroci = element.childNodes;
    var rowId = element.id;
    if(otroci[1].innerHTML == "Strošek"){
      var pot = "/dodajMesecniStrosekOdliv/"+localStorage.getItem("idUporabnika")+"/strosek/"+rowId+'/mesec/'+localStorage.getItem("mesec");
        var form = document.createElement("form");
        form.setAttribute("method","post");
        form.setAttribute("action",pot);
        
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeRacunovMesecniStrosekPotrdi");
        hiddenField.setAttribute("value",otroci[2].getAttribute("value"));
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeImeMesecniStrosekPotrdi");
        hiddenField.setAttribute("value",otroci[0].innerHTML,);
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeKolicinaMesecniStrosekPotrdi");
        hiddenField.setAttribute("value",0);
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","datumStroskaModalMesecniStrosekPotrdi");
        hiddenField.setAttribute("value",'01-01-2017');
        form.appendChild(hiddenField);
      
        document.body.appendChild(form);
        form.submit();
        
      
    }else{
      var pot = "/dodajMesecniPrilivPriliv/"+localStorage.getItem("idUporabnika")+"/priliv/"+rowId+'/mesec/'+localStorage.getItem("mesec");
      var form = document.createElement("form");
        form.setAttribute("method","post");
        form.setAttribute("action",pot);
        console.log("hello");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeRacunovMesecniPriliviPotrdi");
        hiddenField.setAttribute("value",otroci[2].getAttribute("value"));
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeImeMesecniPriliviPotrdi");
        hiddenField.setAttribute("value",otroci[0].innerHTML,);
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","urejanjeKolicinaMesecniPriliviPotrdi");
        hiddenField.setAttribute("value",0);
        form.appendChild(hiddenField);
        
        hiddenField = document.createElement("input");
        hiddenField.setAttribute("type","hidden");
        hiddenField.setAttribute("name","datumStroskaModalMesecniPriliviPotrdi");
        hiddenField.setAttribute("value",'01-01-2017');
        form.appendChild(hiddenField);
      
        document.body.appendChild(form);
        form.submit();
      
    }
    
    
  }
  
}
function brisiRacun(idRacuna){
  
  var idUporabnika = localStorage.getItem("idUporabnika");
  idRacuna = idRacuna.substring(0,idRacuna.length-1);
  console.log(idRacuna);
  if(confirm('ali ste prepričani, da želite brisati?')){
    var pot = '/deleteRacun/'+idUporabnika+'/racun/'+idRacuna+'/params/'+localStorage.getItem("mesec");
    var form = document.createElement("form");
        form.setAttribute("method","get");
        form.setAttribute("action",pot);
      
        document.body.appendChild(form);
        form.submit();
  }
}
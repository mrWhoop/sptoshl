function dodajCilj(){
    var dodajCilj = document.getElementById("ciljBlock");
    if(dodajCilj.style.display === "none"){
        dodajCilj.style.display = "block";
    }else{
        dodajCilj.style.display = "none";
    }
}
        
var ctx = document.getElementById("myChart").getContext('2d');

var data = {
        labels: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
        datasets: [{
            label: 'privarčevano',
            data: [0,10,15,30,40,45,50,60,55,80,85],
            backgroundColor: [
                'rgba(0, 0, 0, 0)'
            ],
            borderColor: [
                'rgba(91, 192, 222, 1)'
            ],
            borderWidth: 1,
            pointHoverBackgroundColor:'rgba(91, 192, 222, 1)'
        },
        {
            label: 'plan varčevanja',
            data:[0,16,24,32,40,56,64,72,80,88,96,100],
            backgroundColor:[
                'rgba(0, 0, 0, 0)'
            ],
            borderColor:[
                'rgba(223, 105, 26, 1)'
            ],
            borderWidth: 1,
            pointHoverBackgroundColor:'rgba(223, 105, 26, 1)'
        }]
    }

var options = {
        elements:{
                  line: {
                      tension: 0,
                  }
              },
              
        legend: {
                  display: true,
                  labels: {
                      fontColor: 'rgb(255,255,255)'
                  }
              }
    }

Chart.defaults.global.defaultFontColor = 'white';
Chart.defaults.global.elements.point.backgroundColor = 'rgba(255,0,0,0.1)'

var myChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
});
var apiParametri = {
   streznik: "http://localhost:" + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
    apiParametri.streznik = "https://afternoon-ridge-89903.herokuapp.com";
}

var request = require('request');

/*funkcionalnosti za racune*/

/*var pridobiUporabnika = function(req,res,id,callback){
  var parametriZahteve, pot;
  pot = '/api/uporabniki/' + id;
  
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json:null
  };
  request(
    parametriZahteve,
    function(napaka,odgovor,vsebina){
      if(odgovor.statusCode == 200){
        var podatki = JSON.parse(vsebina);
        callback(req,res,podatki);
      }else{
        return false;
      }
    });
}

var ustvariRacun = function(req,res,podatki,callback){
    var parametriZahteve,pot,posredovaniPodatki;
    console.log(podatki.ime);
    if(podatki.racuni == null){
        pot = '/api/racuni';
    }else{
        pot = '/api/racuni/'+podatki.racuni;
    }
    posredovaniPodatki = {
        "naziv":req.body.imeRacuna,
        "stanje":req.body.zacetnoStanje
    };
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json: posredovaniPodatki
    }
    
    request(parametriZahteve,
    function(napaka,odgovor,vsebina){
      if(odgovor.statusCode == 201){
          console.log(vsebina);
          var nek = vsebina;
          console.log(nek);
          
          callback(req,res,podatki,nek);
      }else{
          console.log("problemi pri ustvarjenju racuna");
      }  
    });
    
}

var posodobiUporabnika = function(req,res,podatki,vsebina){
    console.log(podatki.ime);
    var pot ='/api/uporabniki/'+podatki._id;
    console.log(vsebina._id);
    podatki.racuni = vsebina._id;
    console.log(pot);
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'PUT',
        json: podatki
    }
    request(
        parametriZahteve,
        function(napaka, odgovor, vrnitev){
            if(odgovor.statusCode == 200){
                res.redirect('/home/'+podatki._id+'/mesec/'+req.params.mesec);
            }else{
                console.log("problemi pri koncu");
            }
        });
}

module.exports.shraniNovRacun = function(req,res){
    var id = req.params.idUporabnika;
    pridobiUporabnika(req,res,id,function(req,res,podatki){
        ustvariRacun(req,res,podatki,function(req,res,podatki,vsebina){
            if(podatki.racuni == null){
                posodobiUporabnika(req,res,podatki,vsebina);
            }else{
                res.redirect('/home/'+podatki._id+'/mesec/'+req.params.mesec);
            }
        });
    });
    
}    
module.exports.urediRacun = function(req,res){
    var uporabnikId = req.params.idUporabnika;
    pridobiUporabnika(req,res,uporabnikId,function(req,res,podatki){
        var racunId = req.params.idRacuna;
        var pot = '/api/racuni/'+podatki.racuni+'/racun/'+racunId;
        console.log(req.body.urejanjeImeRacunov);
        console.log(req.body.urejanjeKolicinaRacunov);
        var posredovaniPodatki = {
            "naziv":req.body.urejanjeImeRacunov,
            "stanje":req.body.urejanjeKolicinaRacunov
        };
        var parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'PUT',
            json: posredovaniPodatki
        }
        request(
        parametriZahteve,
        function(napaka, odgovor, vrnitev){
            if(odgovor.statusCode == 200){
                res.redirect('/home/'+podatki._id+'/mesec/'+req.params.mesec);
            }else{
                console.log(odgovor);
            }
        });
    });
    
    
}    

module.exports.deleteRacun = function(req,res){
    var uporabnikId = req.params.idUporabnika;
    pridobiUporabnika(req,res,uporabnikId,function(req,res,podatki){
        var racunId = req.params.idRacuna;
        var pot = '/api/racuni/'+podatki.racuni+'/racun/'+racunId;
        var parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'DELETE',
            json: null
        };
        console.log(pot);
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == '204'){
                   res.redirect('/home/'+podatki._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log(odgovor);
               } 
            });
        
    })
}
var postPrilivEnkratni = function(req,res,prvic,uporabnik,callback){
    var pot,parametriZahteve,posredovaniPodatki;
    
    if(prvic == true){
        pot = '/api/prilivi';
    }else{
        pot = '/api/prilivi/'+uporabnik.prilivi+'/enkratniPriliv';
    }
    posredovaniPodatki = {
        vrsta:'enkratni',
        naziv:req.body.imePreliva,
        kolicina:req.body.kolicinaPreliva,
        racun:req.body.racunPreliva,
        racunCollectionId:uporabnik.racuni
    };
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json:posredovaniPodatki
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
           if(odgovor.statusCode == 201){
               if(prvic == true){
                   callback(vsebina);
               }else{
                    res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }
           }else{
               console.log(odgovor);
           } 
        });
}
var postPrilivMesecni = function(req,res,prvic,uporabnik,callback){
    var pot,parametriZahteve,posredovaniPodatki;
    
    if(prvic == true){
        pot = '/api/prilivi';
    }else{
        pot = '/api/prilivi/'+uporabnik.prilivi+'/mesecniPriliv';
    }
    console.log(uporabnik.racuni);
    posredovaniPodatki = {
        vrsta:'mesecni',
        naziv:req.body.imePreliva,
        kolicina:req.body.kolicinaPreliva,
        racun:req.body.racunPreliva,
        racunCollectionId:uporabnik.racuni
    };
    console.log(posredovaniPodatki);
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json:posredovaniPodatki
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
           if(odgovor.statusCode == 201){
               if(prvic == true){
                    callback(vsebina);
               }else{
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }
           }else{
               console.log(odgovor);
           } 
        });
}
 //dodajanje prilivov
module.exports.dodajPriliv = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var neki = 0;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        console.log(req.body);
        console.log(uporabnik);
        if(uporabnik.prilivi == null){
            if(req.body.enkratniPreliv){
                postPrilivEnkratni(req,res,true,uporabnik,function(vsebina){
                    posodobiUporabnikaPriliv(req,res,vsebina,uporabnik);
                });
            }else{
                postPrilivMesecni(req,res,true,uporabnik,function(vsebina){
                    posodobiUporabnikaPriliv(req,res,vsebina,uporabnik);
                });
            }
        }else{
            if(req.body.enkratniPreliv){
                postPrilivEnkratni(req,res,false,uporabnik);
            }else{
                postPrilivMesecni(req,res,false,uporabnik);
            }
            
        }
  });
}
var posodobiUporabnikaPriliv = function(req,res,sprememba,uporabnik){
    
    console.log(uporabnik.ime);
    var pot ='/api/uporabniki/'+uporabnik._id;
    console.log(sprememba._id);
    uporabnik.prilivi = sprememba._id;
    console.log(pot);
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'PUT',
        json: uporabnik
    }
    request(
        parametriZahteve,
        function(napaka, odgovor, vrnitev){
            if(odgovor.statusCode == 200){
                res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
            }else{
                console.log("problemi pri koncu");
            }
        });

}



//dodajanje stroskov
module.exports.dodajStrosek = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var neki = 0;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        console.log(req.body);
        console.log(uporabnik);
        if(uporabnik.stroski == null){
            if(req.body.checkboxEnkratni){
                postStrosekEnkratni(req,res,true,uporabnik,function(vsebina){
                    posodobiUporabnikaStrosek(req,res,vsebina,uporabnik);
                });
            }else{
                postStrosekMesecni(req,res,true,uporabnik,function(vsebina){
                    posodobiUporabnikaStrosek(req,res,vsebina,uporabnik);
                });
            }
        }else{
            if(req.body.checkboxEnkratni){
                postStrosekEnkratni(req,res,false,uporabnik);
            }else{
                postStrosekMesecni(req,res,false,uporabnik);
            }
            
        }
  });
}
var postStrosekEnkratni = function(req,res,prvic,uporabnik,callback){
    var pot,parametriZahteve,posredovaniPodatki;
    
    if(prvic == true){
        pot = '/api/stroski';
    }else{
        pot = '/api/stroski/'+uporabnik.stroski+'/enkratniStrosek';
    }
    posredovaniPodatki = {
        vrsta:'enkratni',
        naziv:req.body.imeStroska,
        kolicina:req.body.kolicinaStroska,
        racun:req.body.strosekIzRacuna,
        racunCollectionId:uporabnik.racuni,
        zadnjiVnosKolicina:req.body.kolicinaStroska
    };
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json:posredovaniPodatki
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
           if(odgovor.statusCode == 201){
               if(prvic == true){
                   callback(vsebina);
               }else{
                    res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }
           }else{
               console.log(odgovor);
           } 
        });
}
var postStrosekMesecni = function(req,res,prvic,uporabnik,callback){
    var pot,parametriZahteve,posredovaniPodatki;
    
    if(prvic == true){
        pot = '/api/stroski';
    }else{
        pot = '/api/stroski/'+uporabnik.stroski+'/mesecniStrosek';
    }
    posredovaniPodatki = {
        vrsta:'mesecni',
        naziv:req.body.imeStroska,
        kolicina:req.body.kolicinaStroska,
        racun:req.body.strosekIzRacuna,
        racunCollectionId:uporabnik.racuni
    };
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json:posredovaniPodatki
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
           if(odgovor.statusCode == 201){
               if(prvic == true){
                    callback(vsebina);
               }else{
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }
           }else{
               console.log(odgovor);
           } 
        });
}
var posodobiUporabnikaStrosek = function(req,res,sprememba,uporabnik){
    
    console.log(uporabnik.ime);
    var pot ='/api/uporabniki/'+uporabnik._id;
    console.log(sprememba._id);
    uporabnik.stroski = sprememba._id;
    console.log(pot);
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'PUT',
        json: uporabnik
    }
    request(
        parametriZahteve,
        function(napaka, odgovor, vrnitev){
            if(odgovor.statusCode == 200){
                res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
            }else{
                console.log("problemi pri koncu");
            }
        });

}

//Urejevanje stroskov in prilivov
module.exports.urediMesecniPriliv = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var parametriZahteve, podatkiZahteve;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        console.log(req.body.urejanjeRacunovMesecniPrilivi);
        podatkiZahteve = {
            naziv: req.body.urejanjeImeMesecniPrilivi,
            kolicina: req.body.urejanjeKolicinaMesecniPrilivi,
            racun: req.body.urejanjeRacunovMesecniPrilivi,
        }
        console.log(podatkiZahteve);
        var pot = '/api/prilivi/'+uporabnik.prilivi+'/mesecniPriliv/'+req.params.idPriliv;
        parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'PUT',
            json:podatkiZahteve
        }
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == 200){
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log("problemi");
               } 
            });
    });
    
}
module.exports.urediMesecniStrosek = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var parametriZahteve, podatkiZahteve;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        
        podatkiZahteve = {
            naziv: req.body.urejanjeImeMesecniStrosek,
            kolicina: req.body.urejanjeKolicinaMesecniStrosek,
            racun: req.body.urejanjeRacunovMesecniStrosek,
        }
        console.log(podatkiZahteve);
        var pot = '/api/stroski/'+uporabnik.stroski+'/mesecniStrosek/'+req.params.idStroska;
        parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'PUT',
            json:podatkiZahteve
        }
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == 200){
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log(odgovor);
               } 
            });
    });
}
module.exports.urediEnkratniPriliv = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var parametriZahteve, podatkiZahteve;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        podatkiZahteve = {
            naziv: req.body.urejanjeImeEnkratniPrilivi,
            kolicina: req.body.urejanjeKolicinaEnkratniPrilivi,
            racun: req.body.urejanjeRacunovEnkratniPrilivi,
        }
        var pot = '/api/prilivi/'+uporabnik.prilivi+'/enkratniPriliv/'+req.params.idPriliv;
        parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'PUT',
            json:podatkiZahteve
        }
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == 200){
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log("problemi");
               } 
            });
    });
}
module.exports.urediEnkratniStrosek = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var parametriZahteve, podatkiZahteve;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        console.log(req.body.urejanjeRacunovEnkratniStroski);
        console.log(req.body);
        podatkiZahteve = {
            naziv: req.body.urejanjeImeEnkratniStroski,
            kolicina: req.body.urejanjeKolicinaEnkratniStroski,
            racun: req.body.urejanjeRacunovEnkratniStroski,
            zadnjiVnosKolicina: req.body.datumStroskaModalEnkratniStroski
        }
        var pot = '/api/stroski/'+uporabnik.stroski+'/enkratniStrosek/'+req.params.idStroska;
        parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'PUT',
            json:podatkiZahteve
        }
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == 200){
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log("problemi");
               } 
            });
    });
}
// +1 function
module.exports.dodajEnega = function(req,res){
    var iduporabnika = req.params.idUporabnika;
    var idStroska = req.params.idStroska;
    var parametriZahteve, podatkiZahteve;
    pridobiUporabnika(req,res,iduporabnika,function(req,res,uporabnik){
        podatkiZahteve = {
            kolicina: req.body.kolicina,
            zadnjiVnosKolicina: req.body.enota,
            racun: req.body.poslaniRacun,
            racunCollectionId: uporabnik.racuni
        }
        var pot = '/api/stroski/'+uporabnik.stroski+'/enkratniStrosek/'+idStroska;
        parametriZahteve = {
            url: apiParametri.streznik + pot,
            method: 'POST',
            json: podatkiZahteve
        }
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
               if(odgovor.statusCode == 200){
                   res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
               }else{
                   console.log("problemi");
               } 
                
            });
    });
}
//brisanje stroskov in prilivov
module.exports.deleteMesecniPriliv = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var idPriliva = req.params.idPriliv;
    var parametriZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
       var pot = '/api/prilivi/'+uporabnik.prilivi+'/mesecniPriliv/'+idPriliva;
       parametriZahteve = {
           url: apiParametri.streznik+pot,
           method: 'DELETE',
           json:null
       };
       request(
           parametriZahteve,
           function(napaka, odgovor, vsebina) {
              if(odgovor.statusCode == 204){
                  res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
              }else{
                  console.log(odgovor);
              } 
           });
    });
    
    
}

module.exports.deleteEnkratniPriliv = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var idPriliva = req.params.idPriliv;
    var parametriZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
       var pot = '/api/prilivi/'+uporabnik.prilivi+'/enkratniPriliv/'+idPriliva;
       parametriZahteve = {
           url: apiParametri.streznik+pot,
           method: 'DELETE',
           json:null
       };
       request(
           parametriZahteve,
           function(napaka, odgovor, vsebina) {
              if(odgovor.statusCode == 204){
                  res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
              }else{
                  console.log(odgovor);
              } 
           });
    });

}

module.exports.deleteMesecniStrosek = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var idStroska = req.params.idStroska;
    var parametriZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
       var pot = '/api/stroski/'+uporabnik.stroski+'/mesecniStrosek/'+idStroska;
       parametriZahteve = {
           url: apiParametri.streznik+pot,
           method: 'DELETE',
           json:null
       };
       request(
           parametriZahteve,
           function(napaka, odgovor, vsebina) {
              if(odgovor.statusCode == 204){
                  res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
              }else{
                  console.log(odgovor);
              } 
           });
    });
}

module.exports.deleteEnkratniStrosek = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var idStroska = req.params.idStroska;
    var parametriZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
       var pot = '/api/stroski/'+uporabnik.stroski+'/enkratniStrosek/'+idStroska;
       parametriZahteve = {
           url: apiParametri.streznik+pot,
           method: 'DELETE',
           json:null
       };
       request(
           parametriZahteve,
           function(napaka, odgovor, vsebina) {
              if(odgovor.statusCode == 204){
                  res.redirect('/home/'+uporabnik._id+'/mesec/'+req.params.mesec);
              }else{
                  console.log(odgovor);
              } 
           });
    });
}

//posodobi uporabnika
module.exports.updateUser = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var parametriZahteve;
    
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
        var pot = '/api/uporabniki/'+uporabnik._id;
        uporabnik.ime = req.body.uporabnikIme;
        uporabnik.priimek = req.body.uporabnikPriimek;
        uporabnik.email = req.body.uporabnikEmail;
        console.log(uporabnik);
        parametriZahteve = {
            url: apiParametri.streznik+pot,
            method:'PUT',
            json:uporabnik
        };
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
                if(odgovor.statusCode == 200){
                    res.redirect('/profil/'+idUporabnika+'/mesec/'+req.params.mesec);
                }else{
                    console.log(odgovor);
                }
            });
    });
}
//izbrisi uporabnika
module.exports.deleteUser = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var parametriZahteve;
    var pot = '/api/uporabniki/'+idUporabnika;
    parametriZahteve = {
        url: apiParametri.streznik+pot,
        method:'DELETE',
        json:null
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
            if(odgovor.statusCode == 204){
                res.redirect('/');
            }else{
                console.log(odgovor);
            }
        })
}
module.exports.potrdiMesecniStrosek = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var parametriZahteve,podatkiZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req,res,uporabnik){
      var pot = '/api/stroski/'+uporabnik.stroski+'/mesecniStrosek/'+req.params.idStroska;
      console.log(req.body);
      podatkiZahteve = {
          naziv: req.body.urejanjeImeMesecniStrosekPotrdi,
          kolicina: req.body.urejanjeKolicinaMesecniStrosekPotrdi,
          racun: req.body.urejanjeRacunovMesecniStrosekPotrdi,
          racunCollectionId: uporabnik.racuni
      } 
      parametriZahteve = {
          url: apiParametri.streznik + pot,
          method: 'POST',
          json: podatkiZahteve,
      };
      request(
          parametriZahteve,
          function(napaka, odgovor, vsebina) {
             if(odgovor.statusCode == 200){
                 res.redirect('/home/'+idUporabnika+'/mesec/'+req.params.mesec);
             }else{
                 console.log("problemi");
             } 
          });
    });
}

module.exports.potrdiMesecniPriliv = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var parametriZahteve,podatkiZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req,res,uporabnik){
      var pot = '/api/prilivi/'+uporabnik.prilivi+'/mesecniPriliv/'+req.params.idPriliv;
      console.log(req.body);
      podatkiZahteve = {
          naziv: req.body.urejanjeImeMesecniPriliviPotrdi,
          kolicina: req.body.urejanjeKolicinaMesecniPriliviPotrdi,
          racun: req.body.urejanjeRacunovMesecniPriliviPotrdi,
          racunCollectionId: uporabnik.racuni
      } 
      parametriZahteve = {
          url: apiParametri.streznik + pot,
          method: 'POST',
          json: podatkiZahteve,
      };
      request(
          parametriZahteve,
          function(napaka, odgovor, vsebina) {
             if(odgovor.statusCode == 200){
                 res.redirect('/home/'+idUporabnika+'/mesec/'+req.params.mesec);
             }else{
                 console.log("problemi");
             } 
          });
    });
}

module.exports.dodajCilj = function(req,res){
    var idUporabnika = req.params.idUporabnika;
    var mesec = req.params.mesec;
    var parametriZahteve,podatkiZahteve;
    pridobiUporabnika(req,res,idUporabnika,function(req, res, uporabnik) {
        console.log(req.body);
        if(uporabnik.cilji == null){
            var pot = '/api/cilji';
            if(req.body.checkBoxMesecni){
                podatkiZahteve = {
                    vrsta:"mesecni",
                    naziv: req.body.inputDefault,
                    mesecniOdliv: req.body.kolicinaCilja,
                    
                }
                parametriZahteve = {
                    url: apiParametri.streznik + pot,
                    method: 'POST',
                    json:podatkiZahteve
                };
                request(
                    parametriZahteve,
                    function(napaka, odgovor, vsebina) {
                       if(odgovor.statusCode == 201){
                           posodobiUporabnikaCilj(req,res,vsebina,mesec,uporabnik);
                       }else{
                           console.log("Problem pri dodajanju cilja");
                       }
                    });
            }else{
                podatkiZahteve = {
                    vrsta:"obdobje",
                    naziv: req.body.inputDefault,
                    odDatum: req.body.datumCiljaOd,
                    doDatum: req.body.datumCiljaDo,
                    skupno: req.body.kolicinaCilja,
                }
                parametriZahteve = {
                    url: apiParametri.streznik + pot,
                    method: 'POST',
                    json:podatkiZahteve
                };
                request(
                    parametriZahteve,
                    function(napaka, odgovor, vsebina) {
                       if(odgovor.statusCode == 201){
                           posodobiUporabnikaCilj(req,res,vsebina,mesec,uporabnik);
                       }else{
                           console.log("Problem pri dodajanju cilja");
                       }
                    });
            }
            
        }else{
            if(req.body.checkBoxMesecni){
                var pot = '/api/cilji/'+uporabnik.cilji+'/mesecnicilj';
                podatkiZahteve = {
                    naziv: req.body.inputDefault,
                    mesecniOdliv: req.body.kolicinaCilja,
                };
                parametriZahteve = {
                    url: apiParametri.streznik + pot,
                    method: 'POST',
                    json:podatkiZahteve,
                };
                request(
                    parametriZahteve,
                    function(napaka,odgovor,vsebina){
                        if(odgovor.statusCode == 201){
                            res.redirect('/statistika/'+idUporabnika+'/mesec/'+mesec);
                        }else{
                            console.log("problemi pri dodajanju mesecnega cilja");
                        }
                    });
                
                
                
            }else{
                var pot = '/api/cilji/'+uporabnik.cilji+'/obdobjecilj';
                podatkiZahteve = {
                    naziv: req.body.inputDefault,
                    odDatum: req.body.datumCiljaOd,
                    doDatum: req.body.datumCiljaDo,
                    skupno: req.body.kolicinaCilja,
                }
                parametriZahteve = {
                    url: apiParametri.streznik + pot,
                    method: 'POST',
                    json: podatkiZahteve,
                };
                request(
                    parametriZahteve,
                    function(napaka,odgovor,vsebina){
                       if(odgovor.statusCode == 201){
                           res.redirect('/statistika/'+idUporabnika+'/mesec/'+mesec);
                       }else{
                           console.log("problemi pri dodajanju obdobjeCilj");
                       } 
                    });
            }
        }
    });
}

var posodobiUporabnikaCilj = function(req,res,sprememba,mesec,uporabnik){
    
    console.log(uporabnik.ime);
    var pot ='/api/uporabniki/'+uporabnik._id;
    console.log(sprememba._id);
    uporabnik.cilji = sprememba._id;
    console.log(pot);
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'PUT',
        json: uporabnik
    }
    console.log(uporabnik);
    request(
        parametriZahteve,
        function(napaka, odgovor, vrnitev){
            if(odgovor.statusCode == 200){
                res.redirect('/statistika/'+uporabnik._id+'/mesec/'+mesec);
            }else{
                console.log("problemi pri koncu");
            }
        });

}*/
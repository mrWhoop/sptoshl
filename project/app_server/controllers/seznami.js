var apiParametri = {
   streznik: "http://localhost:" + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
    apiParametri.streznik = "https://afternoon-ridge-89903.herokuapp.com";
}

var request = require('request');


var pridobiUporabnika = function(req,res,callback){
  var parametriZahteve, pot;
  pot = '/api/uporabniki/' + req.params.idUporabnika;
  
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json:null
  };
  request(
    parametriZahteve,
    function(napaka,odgovor,vsebina){
      if(odgovor.statusCode == 200){
        var podatki = JSON.parse(vsebina);
        console.log("hello")
        callback(req, res, podatki);
      }else{
        prikaziNapako(req,res,odgovor.statusCode)
      }
    });
}
var pridobiRacuneOdUporabnika = function(racunId,callback){
  var parametriZahteve, pot,podatki;
  pot = '/api/racuni/' + racunId;
  
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json:null
  };
  request(
    parametriZahteve,
    function(napaka,odgovor,vsebina){
      if(odgovor.statusCode == 200){
        podatki = JSON.parse(vsebina);
        callback(podatki);
      }else{
        callback(null);
      }
    });
}
var pridobiStroskeOdUporabnika = function(strosekId, callback){
  var parametriZahteve,pot,podatki;
  pot = '/api/stroski/'+strosekId;
  
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method:'GET',
    json:null
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
        if(odgovor.statusCode == 200){
          podatki = JSON.parse(vsebina);
          callback(podatki);
        }else{
          callback(null);
        }
    });
  
}
var pridobiPriliveOdUporabnika = function(prilivId,callback){
  var parametriZahteve,pot,podatki;
  pot = '/api/prilivi/'+prilivId;
  
  parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: null
    
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
        if(odgovor.statusCode == 200){
          podatki = JSON.parse(vsebina);
          callback(podatki);
        }else{
          console.log(odgovor);
          callback(null);
        }
    });
  
}

module.exports.seznami = function(req, res) {
    var object = req.params.idUporabnika;
    pridobiUporabnika(req,res,function(zahteva, odgovor, vsebina){
      console.log(vsebina);
      var racunId = vsebina.racuni;
      var priliviId = vsebina.prilivi;
      var stroskiId = vsebina.stroski;
      //ciljiId = vsebina[0].cilji;
      console.log("to je racunID"+racunId);
      var racuniVsi = [];
      var kolekcijaEnkratniPrilivi = [];
      var kolekcijaMesecniPrilivi = [];
      var kolekcijaPriliv = [];
      var kolekcijaEnkratniStroski = [];
      var kolekcijaMesecniStroski = [];
      var kolekcijaStroskov = [];
      var ciljiMesecni = [];
      var ciljiObdobje = [];
      var meseci = ["Januar","Februar","Marec","April","Maj","Junij","Julij","August","September","Oktober","November","December"]
      var mesec = req.params.mesec;
      console.log(mesec);
      var dat = new Date();
      var trenutniMesec = dat.getMonth()+1;
      if(trenutniMesec < 10){
        trenutniMesec = '0'+trenutniMesec;
      }
      console.log("trenutni:"+trenutniMesec);
        //pridobivanje racunov od uporabnika
      pridobiRacuneOdUporabnika(racunId,function(podatki){
        if(podatki != null){
          racuniVsi = podatki.racuni;
        }
        
      
      
          //pridobivanje stroskov in prelivov
        pridobiPriliveOdUporabnika(priliviId,function(prilivKolekcija){
          if(prilivKolekcija != null){
            
            var i = 0;
            var d = 0;
            if(mesec < 10){
              mesec = '0'+mesec;
            }
            console.log("to je mesec:"+mesec);
            for(i = 0; i<prilivKolekcija.enkratniPrilivi.length;i++){
              var devide = prilivKolekcija.enkratniPrilivi[i].datum.split('T');
              prilivKolekcija.enkratniPrilivi[i].datum = devide[0];
              var trenutniDat = prilivKolekcija.enkratniPrilivi[i].datum;
              var razdeljen = trenutniDat.split('-');
              console.log("to je od Priliva:"+razdeljen);
              if(razdeljen[1] == mesec){
                console.log("Ali pride cez");
                kolekcijaPriliv[d] = prilivKolekcija.enkratniPrilivi[i];    
                d++;
              }
            }
            kolekcijaEnkratniPrilivi = kolekcijaPriliv;
            //da najdemo tisto kar je v tem trenutnem mesecu
            var j = 0;
            var d = 0;
            
            for(i = 0; i<prilivKolekcija.mesecniPrilivi.length; i++){
              for(j = 0; j<prilivKolekcija.mesecniPrilivi[i].prilivi.length;j++){
                
                var devide = prilivKolekcija.mesecniPrilivi[i].prilivi[j].datum.split("T");
                prilivKolekcija.mesecniPrilivi[i].prilivi[j].datum = devide[0];
                var trenutniDat = prilivKolekcija.mesecniPrilivi[i].prilivi[j].datum;
                console.log(trenutniDat);
                var razdeljen = trenutniDat.split('-');
                console.log(razdeljen[1]);
                console.log(mesec);
                console.log(razdeljen[1]);
                if(razdeljen[1] == mesec){
                  
                  kolekcijaMesecniPrilivi[d] = prilivKolekcija.mesecniPrilivi[i];
                  kolekcijaMesecniPrilivi[d].kolicina = 0;
                  kolekcijaMesecniPrilivi[d].kolicina = Number(kolekcijaMesecniPrilivi[d].kolicina)+Number(prilivKolekcija.mesecniPrilivi[i].prilivi[j].kolicina);
                  kolekcijaMesecniPrilivi[d].racun = prilivKolekcija.mesecniPrilivi[i].prilivi[j].racun;
                  kolekcijaMesecniPrilivi[d].racunCollectionId = prilivKolekcija.mesecniPrilivi[i].prilivi[j].racunCollectionId;
                  kolekcijaMesecniPrilivi[d].prilivi[0].datum = prilivKolekcija.mesecniPrilivi[i].prilivi[j].datum;
                  d++;
                }else if(mesec == trenutniMesec){
                  kolekcijaMesecniPrilivi[d] = prilivKolekcija.mesecniPrilivi[i];
                  kolekcijaMesecniPrilivi[d].kolicina = 0;
                  kolekcijaMesecniPrilivi[d].kolicina = Number(prilivKolekcija.mesecniPrilivi[i].prilivi[j].kolicina);
                  kolekcijaMesecniPrilivi[d].racun = prilivKolekcija.mesecniPrilivi[i].prilivi[j].racun;
                  kolekcijaMesecniPrilivi[d].racunCollectionId = prilivKolekcija.mesecniPrilivi[i].prilivi[j].racunCollectionId;
                  kolekcijaMesecniPrilivi[d].prilivi[0].datum = "Nepotrjeno";
                }
              }
            }
            
          }
          
          pridobiStroskeOdUporabnika(stroskiId,function(strosekKolekcija){
            if(strosekKolekcija != null){
              console.log(strosekKolekcija);
              kolekcijaStroskov = strosekKolekcija;
              var i = 0;
              var j = 0;
              var d = 0;
              var trenutni = -1;
              for(i = 0; i<strosekKolekcija.enkratniStroski.length;i++){
                for(j = 0; j<strosekKolekcija.enkratniStroski[i].odlivi.length; j++){
                  var devide = strosekKolekcija.enkratniStroski[i].odlivi[j].datum.split("T");
                  strosekKolekcija.enkratniStroski[i].odlivi[j].datum = devide[0];
                  var trenutniDat = strosekKolekcija.enkratniStroski[i].odlivi[j].datum;
                  var razdeljen = trenutniDat.split("-");
                  
                  if(razdeljen[1] == mesec){
                    
                    if(trenutni != d){
                      kolekcijaEnkratniStroski[d] = strosekKolekcija.enkratniStroski[i];
                      kolekcijaEnkratniStroski[d].kolicina = 0;
                      trenutni = d;
                    }
                    kolekcijaEnkratniStroski[d].kolicina = Number(kolekcijaEnkratniStroski[d].kolicina)+Number(strosekKolekcija.enkratniStroski[i].odlivi[j].kolicina);
                    kolekcijaEnkratniStroski[d].racun = strosekKolekcija.enkratniStroski[i].odlivi[j].racun;
                    kolekcijaEnkratniStroski[d].odlivi[0].datum = strosekKolekcija.enkratniStroski[i].odlivi[j].datum;
                  }else if(mesec == trenutniMesec){
                    if(trenutni != d){
                      kolekcijaEnkratniStroski[d] = strosekKolekcija.enkratniStroski[i];
                      kolekcijaEnkratniStroski[d].kolicina = 0;
                      trenutni = d;
                    }
                    kolekcijaEnkratniStroski[d].racun = strosekKolekcija.enkratniStroski[i].odlivi[j].racun;
                    kolekcijaEnkratniStroski[d].odlivi[0].datum = strosekKolekcija.enkratniStroski[i].odlivi[j].datum; 
                  }
                  
                  
                }
                d++;
              }
              
              //da najdemo tisto kar je trenutno v tem mesecu stroski
              i = 0;
              var j = 0;
              var d = 0;
              for(i = 0; i<strosekKolekcija.mesecniStroski.length; i++){
                for(j = 0; j<strosekKolekcija.mesecniStroski[i].odlivi.length;j++){
                  var devide = strosekKolekcija.mesecniStroski[i].odlivi[j].datum.split("T");
                  strosekKolekcija.mesecniStroski[i].odlivi[j].datum = devide[0];
                  var trenutniDat = strosekKolekcija.mesecniStroski[i].odlivi[j].datum;
                  var razdeljen = trenutniDat.split('-');
                  if(razdeljen[1] == mesec){
                    kolekcijaMesecniStroski[d] = strosekKolekcija.mesecniStroski[i];
                    kolekcijaMesecniStroski[d].kolicina = 0;
                    console.log(Number(strosekKolekcija.mesecniStroski[i].odlivi[j].kolicina));
                    kolekcijaMesecniStroski[d].kolicina = Number(kolekcijaMesecniStroski[d].kolicina)+Number(strosekKolekcija.mesecniStroski[i].odlivi[j].kolicina);
                    kolekcijaMesecniStroski[d].racun = strosekKolekcija.mesecniStroski[i].odlivi[j].racun;
                    kolekcijaMesecniStroski[d].racunCollectionId = strosekKolekcija.mesecniStroski[i].odlivi[j].racunCollectionId;
                    kolekcijaMesecniStroski[d].odlivi[0].datum = strosekKolekcija.mesecniStroski[i].odlivi[j].datum;
                    d++;
                  }else if(mesec == trenutniMesec){
                    kolekcijaMesecniStroski[d] = strosekKolekcija.mesecniStroski[i];
                    kolekcijaMesecniStroski.kolicina = 0;
                    kolekcijaMesecniStroski[d].kolicina = Number(strosekKolekcija.mesecniStroski[i].odlivi[j].kolicina);
                    kolekcijaMesecniStroski[d].racun = strosekKolekcija.mesecniStroski[i].odlivi[j].racun;
                    kolekcijaMesecniStroski[d].racunCollectionId = strosekKolekcija.mesecniStroski[i].odlivi[j].racunCollectionId;
                    kolekcijaMesecniStroski[d].odlivi[0].datum = "Nepotrjeno";
                    
                  }
                }
              }
              
            }
            pridobiCilje(req,res,vsebina,function(cilji){
              if(cilji != null){
                console.log(podatki);
                var nekCilj = JSON.parse(cilji);
                console.log(nekCilj);
                ciljiMesecni = nekCilj.varcevalniCiljiMesecni;
              
              
                ciljiObdobje = nekCilj.varcevalniCiljiObdobje;
                console.log(ciljiMesecni);
                console.log(ciljiObdobje);
              }
              console.log(kolekcijaMesecniPrilivi);
              mesec = req.params.mesec;
              trenutniMesec = dat.getMonth()+1;
              odgovor.render('index', {
                  title: 'MojToshl',
                  upr:vsebina,
                  prikaziMesec:meseci[mesec-1],
                  mesec,
                  trenutniMesec,
                  racuni:racuniVsi,
                  kolekcijaMesecniPrilivi,
                  kolekcijaEnkratniPrilivi,
                  kolekcijaEnkratniStroski,
                  kolekcijaMesecniStroski,
                  ciljMesecni:ciljiMesecni,
                  ciljObdobje:ciljiObdobje,
                  id:object
                        
                });
              });
            });
          });  
        });
      });
    
};

module.exports.seznamiMobi = function(req, res) {
    var object = req.params.idUporabnika;
    pridobiUporabnika(req,res,function(zahteva, odgovor, vsebina){
      var racunId = vsebina.racuni;
      var racuniVsi = [];
        //pridobivanje racunov od uporabnika
      pridobiRacuneOdUporabnika(racunId,function(podatki){
        if(podatki != null){
          racuniVsi = podatki.racuni;
        }
        res.render('racuni_mobi', {
            title: 'MojToshl',
            id:object,
            mesec:req.params.mesec,
            racuni:racuniVsi
          });
      });
    });
}

module.exports.seznamVarcevanj = function(req, res){
  var object = req.params.idUporabnika;
  var ciljiObdobje = [];
  var ciljiMesecni = [];
  pridobiUporabnika(req,res,function(zahteva, odgovor, vsebina) {
        console.log(vsebina);
        if(vsebina.cilji != null){
          
          pridobiCilje(req,res,vsebina,function(podatki){
            
              console.log(podatki);
              var nekCilj = JSON.parse(podatki);
              console.log(nekCilj);
              ciljiMesecni = nekCilj.varcevalniCiljiMesecni;
            
            
              ciljiObdobje = nekCilj.varcevalniCiljiObdobje;
            console.log(ciljiMesecni);
            console.log(ciljiObdobje);
          
          
            res.render('statistika', {
              title: 'MojToshl',
              mesec:req.params.mesec,
              id:object,
              varcniCiljiMesecni:ciljiMesecni,
              varcniCiljiObdobje:ciljiObdobje,
            });
          });
        }else{
          
          res.render('statistika', {
            title: 'MojToshl',
            mesec:req.params.mesec,
            id:object,
            varcniCiljiMesecni:ciljiMesecni,
            varcniCiljiObdobje:ciljiObdobje,
          });
        }
  });
};

//obdelava napak
var prikaziNapako = function(zahteva, odgovor, kodaStatusa) {
   var naslov, vsebina;
   if (kodaStatusa == 404) {
     naslov = "404, strani ni mogoče najti.";
     vsebina = "Hmm, kako je to mogoče? Nekaj je šlo narobe.";
   } else {
     naslov = kodaStatusa + ", nekaj je šlo narobe.";
     vsebina = "Nekaj nekje očitno ne deluje.";
   }
   odgovor.status(kodaStatusa);
   odgovor.render('genericno-besedilo', {
     title: naslov,
     vsebina: vsebina
   });
 }
 
module.exports.profil = function(req,res){
    var object = req.params.idUporabnika;
    pridobiUporabnika(req,res,function(zahteva, odgovor, vsebina){
      res.render('profil',{title:'MojToshl',
      id:object,
      mesec:req.params.mesec,
      naslov:vsebina.email,
      ime:vsebina.ime,
      priimek:vsebina.priimek
      });
    });
}
var pridobiCilje = function(req,res,uporabnik,callback){
  var object = req.params.idUporabnika;
  console.log(uporabnik);
  var pot = '/api/cilji/'+uporabnik.cilji;
  var parametriZahteve = {
    url: apiParametri.streznik +pot,
    method: 'GET',
    json:null,
  }
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
        if(odgovor.statusCode == 200){
          callback(vsebina);
        }else{
          callback(null);
        }
        
    })
  
}
var express = require('express');
var router = express.Router();
var ctrlZunanjeLokacije = require('../controllers/zunanjeLokacije');
var ctrlSeznami = require('../controllers/seznami.js');
var ctrlglavniKontrolerji = require('../controllers/glavniKontrolerji.js');
var ctrlexpressKontroler = require('../controllers/expressKontroler.js');
/*glavna stran */
/*router.get('/', ctrlexpressKontroler.angularApp);
router.get('/home',ctrlexpressKontroler.angularApp);
router.get('/registracija',ctrlexpressKontroler.angularApp);
router.get('/profil',ctrlexpressKontroler.angularApp);
router.get('/racuni_mobi',ctrlexpressKontroler.angularApp);
router.get('/data',ctrlexpressKontroler.angularApp);
router.get('/phone',ctrlexpressKontroler.angularApp);
router.get('/statistika',ctrlexpressKontroler.angularApp);*/
/*router.get('/home/:idUporabnika/mesec/:mesec', ctrlSeznami.seznami);

/*funkcionalnosti racuni*/
/*router.post('/dodajRacunNov/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.shraniNovRacun)
router.post('/spremeniRacun/:idUporabnika/racun/:idRacuna/params/:mesec',ctrlglavniKontrolerji.urediRacun);
router.get('/deleteRacun/:idUporabnika/racun/:idRacuna/params/:mesec',ctrlglavniKontrolerji.deleteRacun);

/*functionalnosti prilivi*/
/*router.post('/dodajPriliv/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.dodajPriliv);
router.post('/spremeniEnkratniPriliv/:idUporabnika/priliv/:idPriliv/mesec/:mesec',ctrlglavniKontrolerji.urediEnkratniPriliv)
router.post('/spremeniMesecniPriliv/:idUporabnika/priliv/:idPriliv/mesec/:mesec',ctrlglavniKontrolerji.urediMesecniPriliv)
router.get('/deleteMesecni/:idUporabnika/priliv/:idPriliv/mesec/:mesec',ctrlglavniKontrolerji.deleteMesecniPriliv);
router.get('/deleteEnkratni/:idUporabnika/priliv/:idPriliv/mesec/:mesec',ctrlglavniKontrolerji.deleteEnkratniPriliv);
router.post('/dodajMesecniPrilivPriliv/:idUporabnika/priliv/:idPriliv/mesec/:mesec',ctrlglavniKontrolerji.potrdiMesecniPriliv);

/*funkcionalnosti stroski*/
/*router.post('/dodajStrosek/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.dodajStrosek);
router.post('/spremeniEnkratniStrosek/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.urediEnkratniStrosek);
router.post('/spremeniMesecniStrosek/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.urediMesecniStrosek);
router.post('/dodajEnoto/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.dodajEnega);
router.get('/deleteMesecni/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.deleteMesecniStrosek);
router.get('/deleteEnkratni/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.deleteEnkratniStrosek);
router.post('/dodajMesecniStrosekOdliv/:idUporabnika/strosek/:idStroska/mesec/:mesec',ctrlglavniKontrolerji.potrdiMesecniStrosek);

/*funkcionalnosti cilji*/
/*router.post('/dodajCilj/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.dodajCilj);


/* Notranje Lokacije */

/*router za statistiko*/
//router.get('/statistika/:idUporabnika/mesec/:mesec',ctrlSeznami.seznamVarcevanj);

/*router za racune*/
//router.get('/racuni/:idUporabnika/mesec/:mesec',ctrlSeznami.seznamiMobi);

/*router za profil*/
/*router.get('/profil/:idUporabnika/mesec/:mesec',ctrlSeznami.profil);
router.post('/posodobiUporabnika/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.updateUser);
router.get('/deleteUser/:idUporabnika/mesec/:mesec',ctrlglavniKontrolerji.deleteUser);

/* Zunanje Lokacije */

//router za db (vnos zacetnih podatkov)
//router.get('/db',ctrlZunanjeLokacije.db);

//router za prijavo
//router.get('/',ctrlZunanjeLokacije.prijava);

//router.get('/uporabnik',ctrlZunanjeLokacije.pridobiUporabnika);

//routerji za registracijo
//router.get('/registracija',ctrlZunanjeLokacije.registracija);
//router.post('/registracija',ctrlZunanjeLokacije.shraniUporabnika);

//router za pozabljeno geslo
//router.get('/pozabljenoGeslo',ctrlZunanjeLokacije.pozabljenoGeslo);

/*ORBITAL STRIKE*/
//router.post('/deleteAll',ctrlZunanjeLokacije.deleteAll);*/


module.exports = router;

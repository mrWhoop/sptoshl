(function(){
   /*global angular*/ 
    function glavaCtrl($route,$location){
        var podatki = this;
        podatki.odjaviMe = function(){
            localStorage.removeItem('zeton',)
            localStorage.removeItem("idUporabnika");
            localStorage.removeItem("kolekcijaPrilivovId");
            localStorage.removeItem("kolekcijaRacunovId");
            localStorage.removeItem("kolekcijaStroskovId")
            localStorage.removeItem("idKolekcijeCilji");
            $location.path('/');
            $route.reload();
        }
    }
    glavaCtrl.$inject = ['$route','$location'];
    angular
        .module("MojToshl")
        .controller('glavaCtrl',glavaCtrl)
})()
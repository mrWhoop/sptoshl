(function(){
    /* global angular */
    
    var head = function(){
        return{
            templateUrl:'/skupno/directive/glava.pogled.html',
            controller: 'glavaCtrl',
            controllerAs: 'podatki',
            scope:{
                vsebina: '=vsebina'
            }
        };
    };
    angular
        .module("MojToshl")
        .directive('navigacija',head);
})();
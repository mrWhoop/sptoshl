(function() {
  /* global angular */
  
  var mojToshl = function($http) {
    var header = {
          Authorization: 'Bearer ' + localStorage.getItem('zeton')
        }
    var dodajUporabnika = function(podatki) {
      return $http.post('/api/uporabniki', podatki,{headers:header});
    };
    var prijaviMe = function(podatki){
      var body = {
        email: podatki.email,
        geslo: podatki.geslo
      }
      return $http.post('/api/prijava/',podatki);
    }
    //pridobivanje podatkov
    var pridobiUporabnika = function(podatki) {
      return $http.get('/api/uporabniki/'+podatki,{headers:header});
    }
    var pridobiKolekcijoRacunov = function(podatki){
      return $http.get('/api/racuni/'+podatki,{headers:header});
    }
    var pridobiKolekcijoPrilivov = function(podatki){
      return $http.get('/api/prilivi/'+podatki,{headers:header});
    }
    var pridobiKolekcijoStroskov = function(podatki){
      return $http.get('/api/stroski/'+podatki,{headers:header});
    }
    var posljiCilje = function(podatki,tip){
      var pot;
      var poslani;
      if(tip == 'niGa'){
        pot = '/api/cilji';
        if(podatki.mesecniCilj){
          poslani = {
            vrsta: "mesecni",
            naziv: podatki.imeCilja,
            mesecniOdliv: podatki.kolicinaCilja,
          }
        }else{
          poslani = {
            vrsta: "obdobje",
            naziv: podatki.imeCilja,
            odDatum: podatki.odCilj,
            doDatum: podatki.doCilj,
            skupno: podatki.kolicinaCilja,
          }
        }
      }else{
        if(podatki.mesecniCilj){
          poslani = {
            vrsta:"mesecni",
            naziv: podatki.imeCilja,
            mesecniOdliv: podatki.kolicinaCilja,
          }
          pot = '/api/cilji/'+tip+'/mesecnicilj';
        }else{
          poslani = {
            vrsta: "obdobje",
            naziv: podatki.imeCilja,
            odDatum: podatki.odCilj,
            doDatum: podatki.doCilj,
            skupno: podatki.kolicinaCilja,
          }
          pot = '/api/cilji/'+tip+'/obdobjecilj';
        }
      }
      return $http.post(pot,poslani,{headers:header});
    }
    var pridobiCilje = function(podatki){
      return $http.get('/api/cilji/'+podatki,{headers:header});
    }
    
    
    //posiljanje podatkov
    var posljiPriliv = function(podatki,idPrilivaKolekcija,idRacunovKolekcija){
      var poslani;
      var pot;
      if(podatki.mesecni){
        poslani = {
          vrsta:'mesecni',
          naziv:podatki.ime,
          kolicina:podatki.kolicina,
          racun:podatki.racun,
          racunCollectionId:idRacunovKolekcija
        };
        if(idPrilivaKolekcija != 0){
          pot = '/api/prilivi/'+idPrilivaKolekcija+'/mesecniPriliv';
        }else{
          pot = '/api/prilivi';
        }
      }else{
        poslani = {
          vrsta:'enkratni',
          naziv:podatki.ime,
          kolicina:podatki.kolicina,
          racun:podatki.racun,
          racunCollectionId:idRacunovKolekcija
        };
        if(idPrilivaKolekcija != 0){
          pot = '/api/prilivi/'+idPrilivaKolekcija+'/enkratniPriliv';
        }else{
          pot = '/api/prilivi';
        }
      }
      
      return $http.post(pot,poslani,{headers:header});
    }
    var posljiStrosek = function(podatki,idStroskaKolekcija,idRacunovKolekcija){
      console.log(idStroskaKolekcija);
      var poslani;
      var pot;
      if(podatki.mesecni){
        poslani = {
          vrsta: 'mesecni',
          naziv: podatki.ime,
          kolicina: podatki.kolicina,
          racun: podatki.racun,
          racunCollectionId:idRacunovKolekcija,
        };
        if(idStroskaKolekcija != 0){
          pot = '/api/stroski/'+idStroskaKolekcija+'/mesecniStrosek';
        }else{
          pot ='/api/stroski'
        }
        
      }else{
        console.log(podatki)
        poslani = {
          vrsta: 'enkratni',
          naziv: podatki.ime,
          kolicina: podatki.kolicina,
          racun: podatki.racun,
          racunCollectionId:idRacunovKolekcija,
          zadnjiVnosKolicina: podatki.kolicina,
        };
        if(idStroskaKolekcija != 0){
          pot = '/api/stroski/'+idStroskaKolekcija+'/enkratniStrosek'; 
        }else{
          pot = '/api/stroski';
        }
      }
      return $http.post(pot,poslani,{headers:header});
    }
    var posodobiUporabnika = function(idUporabnika,podatki,tip){
      pridobiUporabnika(idUporabnika).then(
        function success(odgovor){
          console.log(tip);
          if(tip == 'Strosek'){
            odgovor.data.stroski = podatki;
          }else if(tip == 'Priliv'){
            odgovor.data.prilivi = podatki;
          }else{
            odgovor.data.racuni = podatki;
          }
          return $http.put('/api/uporabniki/'+idUporabnika,odgovor.data,{headers:header});
        }
        );
    }
    var posodobiUporabnikaCilji = function(idUporabnika,podatki){
      pridobiUporabnika(idUporabnika).then(
        function success(odgovor){
          odgovor.data.cilji = podatki;
          return $http.put('/api/uporabniki/'+idUporabnika,odgovor.data,{headers:header});
        }
        
        )
        
    }
    var posodobiMesecni = function(podatki,kolekcija,tip,id){
      var pot;
      if(tip == 'Strosek'){
        pot = '/api/stroski/'+kolekcija+'/mesecniStrosek/'+id;
        
      }else if(tip == 'Prilivi'){
        pot = '/api/prilivi/'+kolekcija+'/mesecniPriliv/'+id;
      }else if(tip == 'Racun'){
        pot = '/api/racuni/'+kolekcija+'/racun/'+id;
      }
      console.log(podatki)
      console.log(pot);
      console.log(id);
      return $http.put(pot,podatki,{headers:header});
    }
    var posljiRacun = function(podatki,idRacunovKolekcija){
      var pot
      var  poslani = {
        naziv: podatki.naziv,
        stanje: podatki.stanje,
      }
      if(idRacunovKolekcija == 0){
        pot = '/api/racuni';
      }else{
        pot = '/api/racuni/'+idRacunovKolekcija;
      }
      
      return $http.post(pot,poslani,{headers:header});
    }
    var dodajEnoto = function(podatki,idKolekcijeStroskov,idStroska){
      var pot = '/api/stroski/'+idKolekcijeStroskov+'/enkratniStrosek/'+idStroska;
      return $http.post(pot,podatki,{headers:header});
    }
    var brisiMesecniVnos = function(idKolekcije,id,tip){
      var pot;
      console.log(tip);
      if(tip == 'Strosek'){
        pot = '/api/stroski/'+idKolekcije+'/mesecniStrosek/'+id;
      }else if(tip == 'Priliv'){
        pot = '/api/prilivi/'+idKolekcije+'/mesecniPriliv/'+id;
      }else{
        pot = '/api/racuni/'+idKolekcije+'/racun/'+id;
      }
      return $http.delete(pot,{headers:header});
    }
    var brisiEnkratniVnos = function(idKolekcije,id,tip){
      var pot;
      if(tip == 'Strosek'){
        
      }else{
        pot = '/api/prilivi/'+idKolekcije+'/enkratniPriliv/'+id
      }
      
      return $http.delete(pot,{headers:header});
    }
    var urediUporabnika = function(idUporabnika,podatki,uporabnik){
      console.log(uporabnik)
      uporabnik.ime = podatki.ime;
      uporabnik.priimek = podatki.priimek;
      uporabnik.email = podatki.email;
      console.log(uporabnik);
      return $http.put('/api/uporabniki/'+idUporabnika,uporabnik,{headers:header});
    }
    var pobrisiUporabnika = function(idUporabnika){
      var pot = '/api/uporabniki/'+idUporabnika;
      return $http.delete(pot,{headers:header});
    }
    
    return {
      dodajUporabnika: dodajUporabnika,
      prijaviMe: prijaviMe,
      pridobiUporabnika: pridobiUporabnika,
      pridobiKolekcijoRacunov: pridobiKolekcijoRacunov,
      pridobiKolekcijoPrilivov: pridobiKolekcijoPrilivov,
      pridobiKolekcijoStroskov: pridobiKolekcijoStroskov,
      posljiPriliv: posljiPriliv,
      posljiStrosek: posljiStrosek,
      posodobiUporabnika: posodobiUporabnika,
      posljiRacun: posljiRacun,
      dodajEnoto: dodajEnoto,
      brisiMesecniVnos: brisiMesecniVnos,
      brisiEnkratniVnos: brisiEnkratniVnos,
      urediUporabnika: urediUporabnika,
      posljiCilje: posljiCilje,
      pridobiCilje: pridobiCilje,
      posodobiUporabnikaCilji: posodobiUporabnikaCilji,
      posodobiMesecni:posodobiMesecni,
      pobrisiUporabnika: pobrisiUporabnika,
    };
  };
  mojToshl.$inject = ['$http'];
  
  angular
    .module('MojToshl')
    .service('mojToshl', mojToshl);
})();
(function(){
    /*global angular*/
   var filterDatumaPriliv = function(){
       return function(vrednost){
           var aliJeVTemMesecu = 0;
           if(vrednost == undefined || vrednost.length == 0){
               return null;
           }
           
           if(vrednost[0].datum){
               for(var i =0; i<vrednost.length; i++){
                   var noviDatum = new Date(vrednost[i].datum);
                   var mesecDat = noviDatum.getMonth()+1;
                   if(mesecDat == localStorage.getItem('mesec')){
                       var porazdel = vrednost[i].datum.split("T");
                       vrednost[i].datum = porazdel[0];
                       aliJeVTemMesecu = 1;
                       vrednost[i].visible = true;
                   }
                if(aliJeVTemMesecu == 0){
                    vrednost[i].visible = false;
                }   
               }
               
           }else{
               for(var j = 0; j<vrednost.length; j++){
                   for(var i = 0; i < vrednost[j].prilivi.length; i++){
                       var noviDatum = new Date(vrednost[j].prilivi[i].datum);
                       var mesecDat = noviDatum.getMonth()+1;
                       if(mesecDat == localStorage.getItem('mesec')){
                           var porazdel = vrednost[j].prilivi[i].datum.split("T");
                           vrednost[j].prilivi[i].datum = porazdel[0];
                           aliJeVTemMesecu = 1;
                           vrednost[j].visible = true;
                       }
                   }
                   if(aliJeVTemMesecu == 0){
                       vrednost[j].visible = false;
                   }
                   aliJeVTemMesecu = 0;
               }
           }
           return vrednost;
           
           
       }
   } 
    angular
        .module("MojToshl")
        .filter('filterDatumaPriliv',filterDatumaPriliv);
})()
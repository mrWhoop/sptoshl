(function(){
    /*global angular*/
   var filterDatuma = function(){
       return function(vrednost){
           var aliJeVTemMesecu = 0;
           if(vrednost == undefined || vrednost.length == 0){
               return null;
           }
           for(var j = 0; j < vrednost.length; j++){
               for(var i = 0; i < vrednost[j].odlivi.length; i++){
                   var noviDatum = new Date(vrednost[j].odlivi[i].datum);
                   var mesecDat = noviDatum.getMonth()+1;
                   if(mesecDat == localStorage.getItem('mesec')){
                       var porazdel = vrednost[j].odlivi[i].datum.split("T");
                       vrednost[j].odlivi[i].datum = porazdel[0];
                       aliJeVTemMesecu = 1;
                       vrednost[j].visible = true;
                   }
               }
               if(aliJeVTemMesecu == 0){
                   vrednost[j].visible = false;
               }
               aliJeVTemMesecu = 0
           }
           return vrednost;
           
       }
   } 
    angular
        .module("MojToshl")
        .filter('filterDatuma',filterDatuma);
})()
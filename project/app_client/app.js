/* global angular */
(function(){
    angular.module('MojToshl', ['ngRoute']);

function nastavitev($routeProvider,$locationProvider){
    $routeProvider
        .when('/home', {
            templateUrl: 'pogledi/home/index.pogled.html',
            controller: 'glavniPageCtrl',
            controllerAs: 'glava'
        })
        .when('/',{
            templateUrl: 'pogledi/front/prijava.pogled.html',
            controller: 'prijavaCtrl',
            controllerAs: 'podatki'
        })
        .when('/statistika',{
            templateUrl: 'pogledi/home/statistika.pogled.html',   
            controller: 'varcevanjeCtrl',
            controllerAs: 'podatki',
        })
        .when('/phone',{
            templateUrl: 'pogledi/home/racuni_mobi.pogled.html',
            controller: 'glavniPageCtrl',
            controllerAs: 'glava',
        })
        .when('/profil',{
            templateUrl: 'pogledi/home/profil.pogled.html',
            controller: 'profilCtrl',
            controllerAs: 'podatki',
        })
        .when('/registracija',{
            templateUrl: 'pogledi/front/registracija.pogled.html',
            controller: 'registracijaCtrl',
            controllerAs: 'podatki'
        })
        .when('/pozabljenoGeslo',{
            templateUrl: 'pogledi/front/pozabljenoGeslo.pogled.html',
            controller: 'pozabljenoControler',
            controllerAs: 'podatki'
        })
        .when('/data',{
            templateUrl: 'pogledi/front/data.pogled.html'
        })
        .otherwise({redirectTo: '/'});
         $locationProvider.html5Mode(true);
}
angular 
    .module('MojToshl')
    .config(['$routeProvider','$locationProvider', nastavitev]);
})();
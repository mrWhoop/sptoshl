(function() {
    /* global angular*/
    function profilCtrl(mojToshl,$location,$route){
        var podatki = this;
        podatki.smer = 'home';
        mojToshl.pridobiUporabnika(localStorage.getItem('idUporabnika')).then(
          function success(odgovor){
              console.log(odgovor);
              podatki.ime = odgovor.data.ime;
              podatki.priimek = odgovor.data.priimek;
              podatki.email = odgovor.data.email;
              podatki.upr = odgovor.data;
          }  
            
        );
        
        podatki.posiljanjeUporabnika = function(){
            console.log("fgt");
            console.log(podatki.upr);
            podatki.urediUporabnika(localStorage.getItem('idUporabnika'),podatki,podatki.upr);
        }
        podatki.urediUporabnika = function(idUporabnika,noviPodatki,uporabnik){
            console.log(noviPodatki);
            console.log(idUporabnika);
            console.log(uporabnik);
            mojToshl.urediUporabnika(idUporabnika,noviPodatki,uporabnik).then(
                function success(odgovor){
                    console.log(odgovor);
                }    
            )
        }
        podatki.pobrisiUporabnika = function(){
            idUporabnika = localStorage.getItem('idUporabnika');
            
            mojToshl.pobrisiUporabnika(idUporabnika).then(
                    function success(odgovor){
                        localStorage.removeItem('zeton',)
                        localStorage.removeItem("idUporabnika");
                        localStorage.removeItem("kolekcijaPrilivovId");
                        localStorage.removeItem("kolekcijaRacunovId");
                        localStorage.removeItem("kolekcijaStroskovId")
                        localStorage.removeItem("idKolekcijeCilji");
                        
                        $location.path('/');
                        $route.reload();
                    }
                )
        }
    }
    profilCtrl.$inject = ['mojToshl','$location','$route'];
    
    angular
        .module('MojToshl')
        .controller('profilCtrl',profilCtrl)
    
})()
(function (){
    
    /* global angular*/
    
    
   function varcevanje(mojToshl){
       var podatki = this;
       podatki.ciljiMesecni = [];
       podatki.ciljiEnkratni = [];
       podatki.smer = 'home';
       mojToshl.pridobiUporabnika(localStorage.getItem('idUporabnika')).then(
           function success(odgovor){
               console.log(odgovor);
               if(odgovor.data.cilji == null){
                   podatki.ciljiMesecni = [];
                   podatki.ciljiEnkratni = [];
               }else{
                   localStorage.setItem('idKolekcijeCilji',odgovor.data.cilji);
                   mojToshl.pridobiCilje(odgovor.data.cilji).then(
                      function success(odgovor){
                        podatki.ciljiMesecni = odgovor.data.varcevalniCiljiMesecni;
                        podatki.ciljiEnkratni = odgovor.data.varcevalniCiljiObdobje;
                      }   
                       
                    );
               }
           }
       
   
       );
       podatki.odpriCilji = function(){
           var cilj = document.getElementById('ciljBlock');
           if(cilj.style.display == 'none'){
               cilj.style.display = 'block';
           }else{
               cilj.style.display = 'none';
           }
       }
       podatki.posljiCilj = function(){
           console.log("hello");
           if(localStorage.getItem('idKolekcijeCilji') != null){
               podatki.submitCilj(podatki,localStorage.getItem('idKolekcijeCilji'));
           }else{
               console.log(podatki);
               podatki.submitCilj(podatki,'niGa');
           }
           
       }
       podatki.submitCilj = function(podatki,idKolekcijeCilji){
           mojToshl.posljiCilje(podatki,idKolekcijeCilji).then(
                function success(odgovor){
                    if(idKolekcijeCilji == 'niGa'){
                        mojToshl.posodobiUporabnikaCilji(localStorage.getItem('idUporabnika'),odgovor.data._id);
                                
                        
                    }
                    if(podatki.mesecniCilj){
                        podatki.ciljiMesecni[podatki.ciljiMesecni.length] = odgovor.data;
                    }else{
                        podatki.ciljiEnkratni[podatki.ciljiEnkratni.length] = odgovor.data;
                    }
                    podatki.odpriCilji();
                }   
            )
       }
       
   } 
    varcevanje.$inject = ['mojToshl'];
    
    angular
        .module('MojToshl')
        .controller('varcevanjeCtrl',varcevanje)
    
})()
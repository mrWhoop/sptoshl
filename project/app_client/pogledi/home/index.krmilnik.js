(function() {
    /*global angular*/
    
    function glavniPage(mojToshl,$route,$location){
        var glava = this;
        var idUporabnika = localStorage.getItem('idUporabnika');
        var kolekcijaStroskovId = '';
        var kolekcijaPrilivovId = '';
        var kolekcijaRacunovId = '';
        glava.meseci = ['Januar','Februar','Marec','April','Maj','Junij','Julij','Avgust','September','Oktober','November','December'];
        glava.kolekcijaEnkratniStroski = [];
        glava.kolekcijaMesecniStroski = [];
        var datum = new Date();
        var mesec = datum.getMonth()+1;
        glava.mesec = mesec;
        glava.smer = 'home';
        
        console.log(mesec);
        glava.trenutniMesec = glava.mesec;
        localStorage.setItem('mesec',glava.trenutniMesec);
        glava.prikaziMesec = glava.meseci[glava.trenutniMesec-1];
        if(localStorage.getItem('zeton') == null){
            $location.path('/');
            $route.reload();
        }
        localStorage.setItem('kolekcijaStroskovId',0);
        localStorage.setItem('kolekcijaRacunovId',0);
        localStorage.setItem('kolekcijaPrilivovId',0);
        //pridobivanje podatkov
        mojToshl.pridobiUporabnika(idUporabnika).then(
            function success(odgovor){
                kolekcijaStroskovId = odgovor.data.stroski;
                kolekcijaPrilivovId = odgovor.data.prilivi;
                kolekcijaRacunovId = odgovor.data.racuni;
                glava.racuni = [];
                
                if(kolekcijaRacunovId != null){
                    localStorage.setItem('kolekcijaRacunovId',kolekcijaRacunovId);
                    mojToshl.pridobiKolekcijoRacunov(kolekcijaRacunovId).then(
                        function success(odgovor){
                            glava.racuni = odgovor.data.racuni;
                        },
                         function error(odgovor){
                             glava.racuni = [];
                        },
                        
                    );
                }
                if(kolekcijaPrilivovId != null){
                    localStorage.setItem('kolekcijaPrilivovId',kolekcijaPrilivovId);
                    mojToshl.pridobiKolekcijoPrilivov(kolekcijaPrilivovId).then(
                        function success(odgovor){
                             glava.kolekcijaMesecniPrilivi = odgovor.data.mesecniPrilivi;
                             for(var i = 0; i<glava.kolekcijaMesecniPrilivi.length; i++){
                                glava.kolekcijaMesecniPrilivi[i].visible = true;    
                             }
                             glava.kolekcijaEnkratniPrilivi = odgovor.data.enkratniPrilivi;
                             for(var i = 0; i<glava.kolekcijaEnkratniPrilivi.length; i++){
                                 glava.kolekcijaEnkratniPrilivi[i].visible = true;
                             }
                        },    
                    )
                }
                if(kolekcijaStroskovId != null){
                    localStorage.setItem('kolekcijaStroskovId',kolekcijaStroskovId);
                    mojToshl.pridobiKolekcijoStroskov(kolekcijaStroskovId).then(
                        function success(odgovor){
                            console.log(odgovor);
                            glava.kolekcijaMesecniStroski = odgovor.data.mesecniStroski;
                            for(var i = 0; i<glava.kolekcijaMesecniStroski.length; i++){
                                glava.kolekcijaMesecniStroski[i].visible = true; 
                            }
                            glava.kolekcijaEnkratniStroski = odgovor.data.enkratniStroski;
                            for(var i = 0; i<glava.kolekcijaEnkratniStroski.length; i++){
                                glava.kolekcijaEnkratniStroski[i].visible = true;
                            }
                        }    
                    )
                }
                
            }
        );
        
        //levo in desno
        glava.levo = function(){
            glava.trenutniMesec = glava.trenutniMesec-1;
            if(glava.trenutniMesec == 0){
                glava.trenutniMesec = 12;
            }
            localStorage.setItem('mesec',glava.trenutniMesec);
            glava.prikaziMesec = glava.meseci[glava.trenutniMesec-1];
        }
        glava.desno = function(){
            glava.trenutniMesec = Number(glava.trenutniMesec)+1;
            if(glava.trenutniMesec == 13){
                glava.trenutniMesec = 1;
            }
            localStorage.setItem('mesec',glava.trenutniMesec);
            glava.prikaziMesec = glava.meseci[glava.trenutniMesec-1];
            
        }
        // posiljanje prilivov
        glava.posljiPriliv = function(podatkiPriliva){
            var kolekcijaPrilivovId = localStorage.getItem('kolekcijaPrilivovId');
            var kolekcijaRacunovId = localStorage.getItem('kolekcijaRacunovId');
            var idUporabnika = localStorage.getItem('idUporabnika');
            mojToshl.posljiPriliv(podatkiPriliva,kolekcijaPrilivovId,kolekcijaRacunovId).then(
                function success(odgovor){
                    console.log(odgovor);
                    if(kolekcijaPrilivovId == 0){
                       mojToshl.posodobiUporabnika(idUporabnika,odgovor.data._id,'Priliv');
                    }
                    if(podatkiPriliva.mesecni){
                        glava.kolekcijaMesecniPrilivi[glava.kolekcijaMesecniPrilivi.length] = odgovor.data;
                    }else{
                        glava.kolekcijaEnkratniPrilivi[glava.kolekcijaEnkratniPrilivi.length] = odgovor.data;
                    }
                    glava.dodajPriliv();
                }    
            )
        }
        glava.posiljanjePriliva = function(){
            glava.posljiPriliv(glava.podatkiPriliva);
        }
        
        //posiljanje stroskov
        glava.posljiStrosek = function(podatkiStroska){
            var kolekcijaStroskovId = localStorage.getItem('kolekcijaStroskovId');
            var kolekcijaRacunovId = localStorage.getItem('kolekcijaRacunovId');
            var idUporabnika = localStorage.getItem('idUporabnika');
            mojToshl.posljiStrosek(podatkiStroska,kolekcijaStroskovId,kolekcijaRacunovId).then(
                function success(odgovor){
                    console.log(odgovor);
                    if(kolekcijaStroskovId == 0){
                        mojToshl.posodobiUporabnika(idUporabnika,odgovor.data._id,'Strosek');
                    }
                    if(podatkiStroska.mesecni){
                        glava.kolekcijaMesecniStroski[glava.kolekcijaMesecniStroski.length] = odgovor.data;
                    }else{
                        glava.kolekcijaEnkratniStroski[glava.kolekcijaEnkratniStroski.length] = odgovor.data;
                    }
                    glava.dodajStrosek();
                }
                );
        }
        glava.dodajRacun = function(){
            if(document.getElementById('racunBlock').style.display == 'none'){
                document.getElementById('racunBlock').style.display = 'block';
            }else{
                document.getElementById('racunBlock').style.display = 'none';
            }
        }
        glava.dodajPriliv = function(){
            if(document.getElementById('prelivBlock').style.display == 'none'){
                document.getElementById('prelivBlock').style.display = 'block';
            }else{
                document.getElementById('prelivBlock').style.display = 'none';
            }
        }
        glava.dodajStrosek = function(){
            if(document.getElementById('strosekBlock').style.display == 'none'){
                document.getElementById('strosekBlock').style.display = 'block';
            }else{
                document.getElementById('strosekBlock').style.display = 'none';
            }
        }
        glava.posiljanjeStroska = function(){
            glava.posljiStrosek(glava.podatkiStroska);
        }
        
        //posiljanje racunov
        glava.posljiRacun = function(podatkiRacuna){
            var kolekcijaRacunovId = localStorage.getItem('kolekcijaRacunovId');
            var idUporabnika = localStorage.getItem('idUporabnika');
            mojToshl.posljiRacun(podatkiRacuna,kolekcijaRacunovId).then(
             function success(odgovor){
                 console.log(odgovor);
                 if(kolekcijaRacunovId == 0){
                     mojToshl.posodobiUporabnika(idUporabnika,odgovor.data._id,'Racun');
                 }
                    glava.racuni[glava.racuni.length] = odgovor.data;
                    glava.dodajRacun();
             }   
                
            );
        }
        
        glava.posiljanjeRacuna = function(){
            glava.posljiRacun(glava.podatkiRacuna);
        }
        glava.dodajEnoto = function(idElementa){
            console.log(idElementa);
            idElementa = idElementa+'1';
            console.log(idElementa);
            var row = document.getElementById(idElementa).parentElement.parentElement;
            var rowId = row.id;
            console.log(rowId);
            var rowPravi = document.getElementById(rowId).childNodes;
            console.log(rowPravi);
            var idKolekcijeStroskov = localStorage.getItem('kolekcijaStroskovId');
            
            var podatki = {
                kolicina: rowPravi[7].innerHTML,
                zadnjiVnosKolicina: rowPravi[9].childNodes[1].value,
                racun: rowPravi[5].getAttribute("value"),
                racunCollectionId: localStorage.getItem('kolekcijaRacunovId'),
            };
            mojToshl.dodajEnoto(podatki,idKolekcijeStroskov,rowId).then(
                function success(odgovor){
                    console.log(odgovor);
                    //to je treba se popr
                    console.log(rowPravi);
                    rowPravi[7].innerHTML = odgovor.data.kolicina;
                }
            );
            
        }; 
        glava.brisiMesecni = function(idElementa){
            idElementa = idElementa+'2';
            console.log(idElementa);
            var row = document.getElementById(idElementa).parentElement.parentElement;
            var rowId = row.id;
            console.log(rowId);
            var rowPravi = document.getElementById(rowId).childNodes;
            console.log(rowPravi);
            var kolekcijaId;
            var tip;
            console.log(rowPravi[3].innerHTML);
            if(rowPravi[3].innerHTML == 'Strošek'){
                tip = 'Strosek';
                kolekcijaId = localStorage.getItem('kolekcijaStroskovId');
            }else if(rowPravi[3].innerHTML == 'Priliv'){
                console.log("hello");
                tip = 'Priliv';
                kolekcijaId = localStorage.getItem('kolekcijaPrilivovId');
            }else{
                tip = 'Racun';
                kolekcijaId = localStorage.getItem('kolekcijaRacunovId');
            }
            
            mojToshl.brisiMesecniVnos(kolekcijaId,rowId,tip).then(
                function success(odgovor){
                    console.log("Uspesno pobrisano");
                    var elem = document.getElementById(rowId);
                    elem.parentNode.removeChild(elem);
                    
                }
            );
            
        };
        glava.brisiEnkratni = function(idElementa){
            idElementa = idElementa+'2';
            console.log(idElementa);
            var row = document.getElementById(idElementa).parentElement.parentElement;
            var rowId = row.id;
            console.log(rowId);
            var rowPravi = document.getElementById(rowId).childNodes;
            console.log(rowPravi);
            var kolekcijaId;
            var tip;
            if(rowPravi[3].innerHTML == 'Priliv'){
                tip = 'Priliv';
                kolekcijaId = localStorage.getItem('kolekcijaPrilivovId');
                
            }else{
                tip = 'Strosek';
                kolekcijaId = localStorage.getItem('kolekcijaStroskovId');
            }
            mojToshl.brisiEnkratniVnos(kolekcijaId,rowId,tip).then(
                function success(odgovor){
                    var elem = document.getElementById(rowId);
                    elem.parentNode.removeChild(elem);
                }    
            )
            
        };
        
    //prikaz modala
        glava.urediMesecni = function(tip){
            var type;
            var kolekcija;
            var poslano;
            var id;
            //stroski
            if(tip == 'Stroski'){
                type = 'Strosek';
                kolekcija = localStorage.getItem('kolekcijaStroskovId');
                id=document.getElementById("urejanjeImeMesecniStrosek").name;
            }else if(tip == 'Priliv'){
                type = 'Prilivi';
                kolekcija = localStorage.getItem('kolekcijaPrilivovId');
                id=document.getElementById("urejanjeImeMesecniPrilivi").name;
            }else{
                type = 'Racun';
                kolekcija = localStorage.getItem('kolekcijaRacunovId');
                id=document.getElementById("urejanjeImeRacunov").name;
            }
            console.log(id);
            if(tip == 'Stroski' || tip == 'Priliv'){
                poslano = {
                        racun: document.getElementById("urejanjeRacunovMesecni"+type).value,
                        naziv: document.getElementById("urejanjeImeMesecni"+type).value,
                        kolicina: document.getElementById("urejanjeKolicinaMesecni"+type).value,
                    }
            }else{
                poslano = {
                    "naziv":document.getElementById("urejanjeImeRacunov").value,
                    "stanje":document.getElementById("urejanjeKolicinaRacunov").value,
                }
            }
            console.log(poslano);
            mojToshl.posodobiMesecni(poslano,kolekcija,type,id).then(
                    function success(odgovor){
                        console.log("SUCCESS!!!");
                        
                        if(tip == 'Stroski' || tip == 'Priliv'){
            
                            if(tip == 'Stroski'){
                                var novo = document.getElementById(odgovor.data._id).childNodes;
                                novo[1].innerHTML = odgovor.data.naziv;
                                novo[5].value = odgovor.data.racun;
                                glava.showMesecniModal('Strosek');
                            }else{
                                glava.showMesecniModal('Priliv');
                                var novo = document.getElementById(odgovor.data.mesecniPrilivi[0]._id).childNodes;
                                novo[1].innerHTML = odgovor.data.mesecniPrilivi[0].naziv;
                                novo[5].value = odgovor.data.mesecniPrilivi[0].racun;
                            }
                        }else{
                            glava.racuni = odgovor.data.racuni;
                            glava.showMesecniModal('Racun');
                        }
                    }
                );
        }
        glava.showMesecniModal = function(idElementa){
            console.log(idElementa);
            var type = null;
            if(idElementa != 'Strosek' && idElementa != 'Priliv' && idElementa != 'Racun'){
                
                idElementa = idElementa+'1';    
                console.log(idElementa);
                var row = document.getElementById(idElementa).parentElement.parentElement;
                var rowId = row.id;
                console.log(rowId);
                var rowPravi = document.getElementById(rowId).childNodes;
                console.log(rowPravi);
                var kolekcijaId;
                var tip;
                var type
                if(rowPravi[7]){
                    type = rowPravi[3].innerHTML;
                }else{
                    type = 'Racun';
                }
            }
            console.log(rowPravi);
            if(type == 'Strošek' || idElementa == 'Strosek'){
                if(document.getElementById('urejanjeMesecniStrosek').style.display == 'none'){
                    document.getElementById('urejanjeMesecniStrosek').style.display = 'block';
                }else{
                    document.getElementById('urejanjeMesecniStrosek').style.display = 'none'
                }
                console.log(rowPravi);
                if(type == 'Strošek'){
                    
                    document.getElementById('urejanjeImeMesecniStrosek').value = rowPravi[1].innerHTML;
                    document.getElementById('urejanjeImeMesecniStrosek').setAttribute('name',rowId);
                    document.getElementById('urejanjeKolicinaMesecniStrosek').value=rowPravi[7].innerHTML;
                }
                    
            }else if(type == 'Priliv' || idElementa == 'Priliv'){
                if(document.getElementById('urejanjeMesecniPrilivi').style.display == 'none'){
                    document.getElementById('urejanjeMesecniPrilivi').style.display = 'block';
                }else{
                    document.getElementById('urejanjeMesecniPrilivi').style.display = 'none';
                }
                if(type == 'Priliv'){
                    document.getElementById('urejanjeImeMesecniPrilivi').value = rowPravi[1].innerHTML;
                    document.getElementById('urejanjeImeMesecniPrilivi').setAttribute('name',rowId);
                    document.getElementById('urejanjeKolicinaMesecniPrilivi').value = rowPravi[7].innerHTML;
                }
            }else if(type == 'Racun' || idElementa == 'Racun'){
                if(document.getElementById('urejanjeRacunov').style.display == 'none'){
                    document.getElementById('urejanjeRacunov').style.display = 'block';
                }else{
                    document.getElementById('urejanjeRacunov').style.display = 'none';
                }
                if(type == 'Racun'){
                    console.log(rowPravi);
                    document.getElementById("urejanjeImeRacunov").value = rowPravi[1].innerHTML;
                    document.getElementById("urejanjeKolicinaRacunov").value = rowPravi[3].innerHTML;
                    document.getElementById("urejanjeImeRacunov").setAttribute('name',rowId);
                    
                }
            }
        };
    };
    
    
    
    glavniPage.$inject = ['mojToshl','$route','$location'];
    
    angular
        .module('MojToshl')
        .controller('glavniPageCtrl',glavniPage);
})();
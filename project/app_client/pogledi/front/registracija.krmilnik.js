(function() {
  /* global angular */
  registracijaCtrl.$inject = ['mojToshl','$location','$route'];
  function registracijaCtrl(mojToshl,$location,$route) {
    var podatki = this;
    podatki.napaka = "neki";
    podatki.smer = 'neki';
    if(localStorage.getItem('zeton') != null){
          $location.path('/home');
          $route.reload();
    }
    podatki.dodajUporabnika = function(podatkiUporabnika) {
      mojToshl.dodajUporabnika({
        "vloge":["uporabnik"],
        "ime":podatkiUporabnika.ime,
        "priimek":podatkiUporabnika.priimek,
        "email":podatkiUporabnika.email,
        "geslo":podatkiUporabnika.pwd,
        "facebookId":null,
        "racuni":null,
        "prilivi":null,
        "stroski":null,
        "cilji":null
      }).then(
        function success(odgovor) {
          console.log(odgovor);
          localStorage.setItem('zeton',odgovor.data.zeton);
          localStorage.setItem('idUporabnika',odgovor.data.uporabnik._id);
          
          $location.path('/home');
          $route.reload();
          
          
        },
        function error(odgovor){
          podatki.napaka = "vrednost";
        },
      );
      return false;
    }
    
    podatki.posiljanjePodatkov = function() {
      podatki.napakaNaObrazcu = "";
      podatki.dodajUporabnika(podatki.podatkiUporabnika);
      
    };
  };
  
  
  angular
    .module('MojToshl')
    .controller('registracijaCtrl', registracijaCtrl);
})()
    

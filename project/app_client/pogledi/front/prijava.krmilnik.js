(function(){
    /* global angular */

    function prijavaCtrl(mojToshl,$scope,$location,$route){
        var podatki = this;
        podatki.napaka = 'neki';
        podatki.stran = 'neki';
        podatki.smer = 'neki';
        if(localStorage.getItem('zeton') != null){
            $location.path('/home');
            $route.reload();
        }
        podatki.prijaviMe = function(podatkiUporabnik){
            
            mojToshl.prijaviMe(podatkiUporabnik).then(
                function success(odgovor){
                    console.log(odgovor);
                            console.log(odgovor);
                            console.log("SUCCESS!!");
                            console.log(odgovor);
                            localStorage.setItem('zeton',odgovor.data.zeton);
                            localStorage.setItem('idUporabnika',odgovor.data.uporabnik._id);
                            $location.path('/home');
                            $route.reload();
                            
                            
                        
                    },
                    
                function error(odgovor){
                    console.log(odgovor);
                    podatki.napaka = 'neObstaja';
                    podatki.vsebina = odgovor.data.msg;
                },
            );
            return false;
        }
    
        podatki.pridobivanjeUporabnika = function(){
            podatki.prijaviMe(podatki.podatkiUporabnik);
        }
    };
    
    prijavaCtrl.$inject = ['mojToshl','$scope','$location','$route'];
    angular
    .module('MojToshl')
    .controller('prijavaCtrl',prijavaCtrl);
})();
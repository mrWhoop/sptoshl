var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Uporabniki = mongoose.model('Uporabnik');
var FaceBookStrategija = require('passport-facebook').Strategy;

passport.use(new LokalnaStrategija({
    usernameField: 'email',
    passwordField: 'geslo'
    },
    function(uporabniskoIme, geslo, koncano) {
        Uporabniki.findOne(
        { email: uporabniskoIme }, 
            function(napaka, uporabnik) {
                if (napaka) { return koncano(napaka); }
                if (!uporabnik) {
                    return koncano(null, false, {
                        msg: 'Napačno uporabniško ime'
                    });
                }
                if (!uporabnik.preveriGeslo(geslo)) {
                    return koncano(null, false, {
                        msg: 'Napačno geslo'
                    });
                }
                return koncano(null, uporabnik);
            }
        );
    }
));

//FACEBOOK https://stackoverflow.com/questions/14572600/passport-js-restful-auth

passport.use(new FaceBookStrategija({
    clientID: "147063492622151",
    clientSecret: "458615de0e9caf62843d3275a9732c6f",
    callbackURL: "https://afternoon-ridge-89903.herokuapp.com/api/facebook/callback" //to je treba še pogruntat kko.. pa kaj sploh dela..
  },
  function(accessToken, refreshToken, profile, cb) {
    Uporabniki.findOne({
            'facebookId': profile.id 
        }, function(err, user) {
            if (err) {
                return cb(err);
            }
            if (!user) {
                user = new Uporabniki({
                    ime: profile.displayName,
                    email: profile.emails[0].value,
                    facebookId: profile.id
                });
                user.save(function(err) {
                    if (err) console.log(err);
                    return cb(null, user);
                });
            } else {
                //found user. Return
                return cb(null, user);
            }
        });
    }
));
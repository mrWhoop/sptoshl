var express = require('express');
var passport = require('passport');

var jwt = require('express-jwt');
var avtentikacija = jwt({
    secret: process.env.JWT_GESLO,
    userProperty: 'payload'
});

var router = express.Router();

var ctrlUporabniki = require('../controllers/uporabniki');
var ctrlRacuni = require('../controllers/racuni');
var ctrlStroski = require('../controllers/stroski');
var ctrlPrilivi = require('../controllers/prilivi');
var ctrlCilji = require('../controllers/cilji');
var ctrlOrbitalStrike = require('../controllers/orbitalStrike');

var ctrlAvtentikacija = require('../controllers/avtentikacija');


/* !!! SLASI NA ZACEKU URLJEV !!!  */

//DANGER ZONE
router.delete('/fuckall', ctrlOrbitalStrike.armagedon);
//DANGER ZONE

//----------------------------------------------------------
//PRIJAVA --------------------------------------------------DONE
//----------------------------------------------------------

router.post('/prijava',ctrlAvtentikacija.prijava);
router.get('/facebook', ctrlAvtentikacija.facebook);
router.get('/facebook/callback', passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/' }));

//----------------------------------------------------------
//UPORABNIKI -----------------------------------------------DONE
//----------------------------------------------------------

//GET - branje
router.get('/uporabniki', ctrlUporabniki.getAll);
router.get('/uporabniki/:objectId', avtentikacija, ctrlUporabniki.getById);
router.get('/uporabniki/email/:email', ctrlUporabniki.getByEmail);
router.get('/uporabniki/facebook/:facebookId', ctrlUporabniki.getByFacebookId);

//POST - kreiranje(registracija)
router.post('/uporabniki', ctrlUporabniki.createNew);

//PUT - posodobi
router.put('/uporabniki/:objectId', avtentikacija, ctrlUporabniki.updateExisting);

//DELETE - brisi
router.delete('/uporabniki/:objectId', avtentikacija, ctrlUporabniki.deleteById);



//----------------------------------------------------------
//RACUNI ---------------------------------------------------DONE
//----------------------------------------------------------


//GET - branje
router.get('/racuni/:objectId', avtentikacija, ctrlRacuni.getCollectionById); //vraca celo zbirko
router.get('/racuni/:collectionId/racun/:objectId', avtentikacija, ctrlRacuni.getById);

//POST - kreiranje
//ko kolekcija racunov za uporabnika se ne obstaja, po tem klicu nujna posodobitev uporabnika s put in poljem "racuni"
router.post('/racuni', avtentikacija, ctrlRacuni.createNewCollection);
//za dodajanje vseh naslednjih računov v kolekcijo
router.post('/racuni/:collectionId', avtentikacija, ctrlRacuni.createNewRacun);

//PUT - posodobi
router.put('/racuni/:collectionId/racun/:objectId', avtentikacija, ctrlRacuni.updateRacun);

//DELETE - brisi
router.delete('/racuni/:objectId', avtentikacija, ctrlRacuni.deleteCollectionById); //zbrise celotno zbirko, prasi tastare ce loh
router.delete('/racuni/:collectionId/racun/:objectId', avtentikacija, ctrlRacuni.deleteRacun); //zbrise en racun


//----------------------------------------------------------
//PRILIVI --------------------------------------------------DONE
//----------------------------------------------------------


//GET - branje
router.get('/prilivi/:objectId', avtentikacija, ctrlPrilivi.getCollectionById); //vraca celo zbirko
router.get('/prilivi/:collectionId/mesecnipriliv/:objectId', avtentikacija, ctrlPrilivi.getMesecniPrilivById);
router.get('/prilivi/:collectionId/enkratnipriliv/:objectId', avtentikacija, ctrlPrilivi.getEnkratniPrilivById);

//POST - kreiranje
router.post('/prilivi', avtentikacija, ctrlPrilivi.createNewCollection);// ko se ni kolekcije
router.post('/prilivi/:collectionId/mesecnipriliv', avtentikacija, ctrlPrilivi.createMesecniPriliv);
router.post('/prilivi/:collectionId/enkratnipriliv', avtentikacija, ctrlPrilivi.createEnkratniPriliv);
router.post('/prilivi/:collectionId/mesecnipriliv/:objectId', avtentikacija, ctrlPrilivi.createPrilivUpdateMesecniPriliv);

//PUT - posodobi
router.put('/prilivi/:collectionId/mesecnipriliv/:objectId', avtentikacija, ctrlPrilivi.updateMesecniPrilivById);
router.put('/prilivi/:collectionId/enkratnipriliv/:objectId', avtentikacija, ctrlPrilivi.updateEnkratniPrilivById);

//DELETE - brisi
router.delete('/prilivi/:objectId', avtentikacija, ctrlPrilivi.deleteById); //zbrise celotno zbirko, paz da nou fotr znoru
router.delete('/prilivi/:collectionId/mesecnipriliv/:objectId', avtentikacija, ctrlPrilivi.deleteMesecniPrilivById);
router.delete('/prilivi/:collectionId/enkratnipriliv/:objectId', avtentikacija, ctrlPrilivi.deleteEnkratniPrilivById);


//----------------------------------------------------------
//STROSKI --------------------------------------------------DONE
//----------------------------------------------------------


//GET - branje
router.get('/stroski/:objectId', avtentikacija, ctrlStroski.getCollectionById); //vraca celo zbirko
router.get('/stroski/:collectionId/mesecnistrosek/:objectId', avtentikacija, ctrlStroski.getMesecniStrosekById);
router.get('/stroski/:collectionId/enkratnistrosek/:objectId', avtentikacija, ctrlStroski.getEnkratniStrosekById);

//POST - kreiranje
router.post('/stroski', avtentikacija, ctrlStroski.createNewCollection);
router.post('/stroski/:collectionId/mesecnistrosek', avtentikacija, ctrlStroski.createMesecniStrosek);
router.post('/stroski/:collectionId/enkratnistrosek', avtentikacija, ctrlStroski.createEnkratniStrosek);
router.post('/stroski/:collectionId/enkratnistrosek/:objectId', avtentikacija, ctrlStroski.createOdlivUpdateEnkratniStrosek);
router.post('/stroski/:collectionId/mesecnistrosek/:objectId', avtentikacija, ctrlStroski.createOdlivUpdateMesecniStrosek);

//PUT - posodobi
router.put('/stroski/:collectionId/mesecnistrosek/:objectId', avtentikacija, ctrlStroski.updateMesecniStrosekById);
router.put('/stroski/:collectionId/enkratnistrosek/:objectId', avtentikacija, ctrlStroski.updateEnkratniStrosekById);

//DELETE - brisi
router.delete('/stroski/:objectId', avtentikacija, ctrlStroski.deleteById); //zbrise celotno zbirko, ce se tamau spet zacne dret pa metat na tla si si sam kriu
router.delete('/stroski/:collectionId/mesecnistrosek/:objectId', avtentikacija, ctrlStroski.deleteMesecniStrosekById);
router.delete('/stroski/:collectionId/enkratnistrosek/:objectId', avtentikacija, ctrlStroski.deleteEnkratniStrosekById);


//----------------------------------------------------------
//CILJI ----------------------------------------------------DONE
//----------------------------------------------------------


//GET - branje
router.get('/cilji/:objectId', avtentikacija, ctrlCilji.getCollectionById); //vraca celo zbirko
router.get('/cilji/:collectionId/mesecnicilj/:objectId', avtentikacija, ctrlCilji.getMesecniCiljById);
router.get('/cilji/:collectionId/obdobjeCilj/:objectId', avtentikacija, ctrlCilji.getObdobjeCiljById);

//POST - kreiranje
router.post('/cilji', avtentikacija, ctrlCilji.createNewCollection);
router.post('/cilji/:collectionId/mesecnicilj', avtentikacija, ctrlCilji.createMesecniCilj);
router.post('/cilji/:collectionId/obdobjecilj', avtentikacija, ctrlCilji.createObdobjeCilj);
router.post('/cilji/:collectionId/mesecnicilj/:objectId', avtentikacija, ctrlCilji.createOdlivUpdateCiljMesecni);
router.post('/cilji/:collectionId/obdobjecilj/:objectId', avtentikacija, ctrlCilji.createOdlivUpdateCiljObdobje);


//PUT - posodobi
router.put('/cilji/:collectionId/mesecnicilj/:objectId', avtentikacija, ctrlCilji.updateMesecniCiljById);
router.put('/cilji/:collectionId/obdobjeCilj/:objectId', avtentikacija, ctrlCilji.updateObdobjeCiljById);

//DELETE - brisi
router.delete('/cilji/:objectId', avtentikacija, ctrlCilji.deleteById); //zbrise celotno zbirko, bot raj tih da noujo doma zvedl
router.delete('/cilji/:collectionId/mesecnicilj/:objectId', avtentikacija, ctrlCilji.deleteMesecniCiljById);
router.delete('/cilji/:collectionId/obdobjeCilj/:objectId', avtentikacija, ctrlCilji.deleteObdobjeCiljById);

//kravamala
module.exports = router;
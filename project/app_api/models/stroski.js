var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var strosekOdliv = new mongoose.Schema({
    kolicina:Number,
    datum:{type: Date, "default": Date.now},
    racun:ObjectId
});

var enkratniStrosekSchema = new mongoose.Schema({
    naziv: {type:String, required:true},
    kolicina: {type:Number, required:true},
    zadnjiVnosKolicina: Number,
    racun: ObjectId,
    odlivi:[strosekOdliv]
});

var mesecniStrosekSchema = new mongoose.Schema({
    naziv: {type:String, required:true},
    kolicina: {type:Number, required:true},
    racun: ObjectId,
    odlivi:[strosekOdliv]
});

var stroskiSchema = new mongoose.Schema({
    enkratniStroski:[enkratniStrosekSchema],
    mesecniStroski:[mesecniStrosekSchema],
});

mongoose.model('Strosek', stroskiSchema, 'Stroski');
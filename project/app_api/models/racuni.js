var mongoose = require('mongoose');

var odlivSchema = new mongoose.Schema({
    kolicina: {type:Number, required:true},
    datum: {type: Date, "default": Date.now}
});

var prilivSchema = new mongoose.Schema({
    kolicina: {type:Number, required:true},
    datum: {type: Date, "default": Date.now}
});

var racunSchema = new mongoose.Schema({
    naziv: {type:String,required:true},
    stanje: {type:Number, required:true},
    odlivi:[odlivSchema],
    prilivi:[prilivSchema]
});

var racuniSchema = new mongoose.Schema({
    racuni:[racunSchema]
});

mongoose.model('Racun', racuniSchema, 'Racuni');
var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.ObjectId;

var priliv = new mongoose.Schema({
    kolicina:Number,
    datum:{type: Date, "default": Date.now},
    racun:ObjectId
});

var enkratniPrilivSchema = new mongoose.Schema({
    naziv: {type:String, required:true},
    kolicina: {type:Number, required:true},
    datum: {type: Date, "default": Date.now},
    racun: ObjectId
});

var mesecniPrilivSchema = new mongoose.Schema({
    naziv: {type:String, required:true},
    kolicina: {type:Number, required:true},
    racun: ObjectId,
    prilivi:[priliv]
});

var priliviSchema = new mongoose.Schema({
    enkratniPrilivi:[enkratniPrilivSchema],
    mesecniPrilivi:[mesecniPrilivSchema],
});

mongoose.model('Priliv', priliviSchema, 'Prilivi');
var mongoose = require('mongoose');

var realniMesecniOdlivSchema = new mongoose.Schema({
    kolicina: {type:Number, required:true},
    datum: {type: Date, "default": Date.now}
});

var varcevalniCiljiMesecniSchema = new mongoose.Schema({
    naziv:{type:String, required:true},
    privarcevano:Number,
    mesecniOdliv:Number,
    ralniMesecniOdliv:[realniMesecniOdlivSchema]
});

var varcevalniCiljiObdobjeSchema = new mongoose.Schema({
    naziv: {type:String, required:true},
    odDatum:Date,
    doDatum:Date,
    skupno:Number,
    privarcevano:Number,
    realniMesecniOdliv:[realniMesecniOdlivSchema]
});

var ciljiSchema = new mongoose.Schema({
    varcevalniCiljiMesecni:[varcevalniCiljiMesecniSchema],
    varcevalniCiljiObdobje:[varcevalniCiljiObdobjeSchema]
});

mongoose.model('Cilj', ciljiSchema, 'Cilji');
var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var ObjectId = mongoose.Schema.ObjectId;

var uporabnikSchema = new mongoose.Schema({
    vloge:[String],
    ime:{type:String, required:true},
    priimek:{type:String, required:true},
    email:{type:String, unique : true, required : true, dropDups: true},
    zgoscenaVrednost: String,
    nakljucnaVrednost: String,
    facebookId:{type:String, required:false},
    racuni:ObjectId,
    prilivi:ObjectId,
    stroski:ObjectId,
    cilji:ObjectId
});

uporabnikSchema.methods.nastaviGeslo = function(geslo) {
    this.nakljucnaVrednost = crypto.randomBytes(16).toString('hex');
    this.zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
};

uporabnikSchema.methods.preveriGeslo = function(geslo) {
    var zgoscenaVrednost = crypto.pbkdf2Sync(geslo, this.nakljucnaVrednost, 1000, 64, 'sha512').toString('hex');
    return this.zgoscenaVrednost == zgoscenaVrednost;
};

uporabnikSchema.methods.generirajJwt = function() {
    var datumPoteka = new Date();
    datumPoteka.setDate(datumPoteka.getDate() + 7);
    
    return jwt.sign({
        _id: this._id,
        elektronskiNaslov: this.elektronskiNaslov,
        ime: this.ime,
        datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
    }, process.env.JWT_GESLO);
};

mongoose.model('Uporabnik', uporabnikSchema, 'Uporabniki');


var mongoose = require('mongoose');
var Cilj = mongoose.model('Cilj');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.getCollectionById = function(zahteva, odgovor) { //vraca celo zbirko
  if (zahteva.params.objectId) {
    Cilj
      .findById(zahteva.params.objectId)
      .exec(function(napaka, cilj) {
        if (!cilj) {
          vrniJsonOdgovor(odgovor, 404, { "msg":"Ne najdem cilja s podanim enoličnim identifikatorjem." });
          return;
        }
        else if(napaka){
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, cilj);
      });    
    }
    else{
      vrniJsonOdgovor(odgovor, 404, { "msg": "Manjka enolični identifikator objectId"});
    }
};

module.exports.getMesecniCiljById = function(zahteva, odgovor){
  //TODO  - ce bo potreba
};

module.exports.getObdobjeCiljById = function(zahteva, odgovor){
  //TODO  - ce bo potreba
};

module.exports.createNewCollection = function(zahteva, odgovor){
  if(zahteva.body.vrsta == "obdobje"){
      Cilj
      .create({
          varcevalniCiljiObdobje:[{
            naziv:zahteva.body.naziv,
            odDatum:zahteva.body.odDatum,
            doDatum:zahteva.body.doDatum,
            skupno:zahteva.body.skupno,
            privarcevano:0
        }]
      },function(napaka, cilj){
          if (napaka){
            vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
            vrniJsonOdgovor(odgovor, 201, cilj);
          }
        }
      );
  }
  else if(zahteva.body.vrsta == "mesecni"){
      Cilj
      .create({
        varcevalniCiljiMesecni:[{
          naziv:zahteva.body.naziv,
          privarcevano:0,
          mesecniOdliv:zahteva.body.mesecniOdliv
        }]
      },function(napaka, cilj){
          if (napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
            vrniJsonOdgovor(odgovor, 201, cilj);
          }
        }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 400, {"msg":"Vrsta cilja ni podana."});
  }
};

module.exports.createMesecniCilj = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Cilj
        .findById(idCollection)
        .select('varcevalniCiljiMesecni')
        .exec(
          function(napaka, cilji) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajVarcevalniCiljMesecni(zahteva, odgovor, cilji);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajVarcevalniCiljMesecni = function(zahteva, odgovor, cilji){
  if (!cilji) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    cilji.varcevalniCiljiMesecni.push({
      naziv:zahteva.body.naziv,
      privarcevano:0,
      mesecniOdliv:zahteva.body.mesecniOdliv
    });
  cilji.save(function(napaka, cilj) {
    var dodaniCilj;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        dodaniCilj = cilji.varcevalniCiljiMesecni[cilji.varcevalniCiljiMesecni.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniCilj);
      }
    });
  }
};

module.exports.createObdobjeCilj = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Cilj
        .findById(idCollection)
        .select('varcevalniCiljiObdobje')
        .exec(
          function(napaka, cilji) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajVarcevalniCiljObdobje(zahteva, odgovor, cilji);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajVarcevalniCiljObdobje = function(zahteva, odgovor, cilji){
  if (!cilji) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    cilji.varcevalniCiljiObdobje.push({
        naziv:zahteva.body.naziv,
        odDatum:zahteva.body.odDatum,
        doDatum:zahteva.body.doDatum,
        skupno:zahteva.body.skupno,
        privarcevano:0
    });
  cilji.save(function(napaka, cilj) {
    var dodaniCilj;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        dodaniCilj = cilji.varcevalniCiljiObdobje[cilji.varcevalniCiljiObdobje.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniCilj);
      }
    });
  }
};

module.exports.createOdlivUpdateCiljMesecni = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idCilj = zahteva.params.objectId;
  
  if (!idCollection || !idCilj) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  
  Cilj
    .findById(idCollection)
    .select('varcevalniCiljiMesecni')
    .exec(
      function(napaka, cilj) {
        var trenutniCilj;
        if (!cilj) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (cilj.varcevalniCiljiMesecni && cilj.varcevalniCiljiMesecni.length > 0) {
          trenutniCilj = cilj.varcevalniCiljiMesecni.id(zahteva.params.objectId);
          if (!trenutniCilj) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniCilj.privarcevano = Number(trenutniCilj.privarcevano) + Number(zahteva.body.mesecniOdliv);
            trenutniCilj.mesecniOdliv = zahteva.body.mesecniOdliv;
            trenutniCilj.realniMesecniOdliv.push({
              kolicina: zahteva.body.mesecniOdliv
            });
            
            cilj.save(function(napaka, trenutniCilj) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniCilj);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.createOdlivUpdateCiljObdobje = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  
  Cilj
    .findById(idCollection)
    .select('varcevalniCiljiObdobje')
    .exec(
      function(napaka, cilj) {
        var trenutniCilj;
        if (!cilj) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija ciljev ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (cilj.varcevalniCiljiObdobje && cilj.varcevalniCiljiObdobje.length > 0) {
          trenutniCilj = cilj.varcevalniCiljiObdobje.id(zahteva.params.objectId);
          if (!trenutniCilj) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema cilja."});
          }
          else {
            trenutniCilj.privarcevano = Number(trenutniCilj.privarcevano) + Number(zahteva.body.mesecniOdliv);
            trenutniCilj.realniMesecniOdliv.push({
              kolicina: zahteva.body.mesecniOdliv
            });
            
            cilj.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniCilj);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateMesecniCiljById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Cilj
    .findById(idCollection)
    .select('varcevalniCiljiMesecni')
    .exec(
      function(napaka, cilj) {
        var trenutniCilj;
        if (!cilj) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija ciljev ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (cilj.varcevalniCiljiMesecni && cilj.varcevalniCiljiMesecni.length > 0) {
          trenutniCilj = cilj.varcevalniCiljiMesecni.id(zahteva.params.objectId);
          if (!trenutniCilj) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniCilj.naziv = zahteva.body.naziv,
            trenutniCilj.skupno = zahteva.body.skupno,
            trenutniCilj.privarcevano = zahteva.body.privarceano,
            trenutniCilj.mesecniOdliv = zahteva.body.mesecniOdliv;
            cilj.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniCilj);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateObdobjeCiljById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Cilj
    .findById(idCollection)
    .select('varcevalniCiljiObdobje')
    .exec(
      function(napaka, cilj) {
        var trenutniCilj;
        if (!cilj) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija ciljev ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (cilj.varcevalniCiljiObdobje && cilj.varcevalniCiljiObdobje.length > 0) {
          trenutniCilj = cilj.varcevalniCiljiObdobje.id(zahteva.params.objectId);
          if (!trenutniCilj) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniCilj.naziv = zahteva.body.naziv,
            trenutniCilj.odDatum = zahteva.body.odDatum,
            trenutniCilj.doDatum = zahteva.body.doDatum,
            trenutniCilj.skupno = zahteva.body.skupno,
            trenutniCilj.privarcevano = zahteva.body.privarcevano;
            cilj.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniCilj);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.deleteById =  function(zahteva, odgovor) { //zbrise celotno zbirki, prasi mt napri
  var idCilja = zahteva.params.objectId; //tole je v resnici cela zbirka
    if (idCilja) {
      Cilj
        .findByIdAndRemove(idCilja)
        .exec(function(napaka, strosek) {
          if (napaka) {
              vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        });
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka ciljev ni podana"});
    }
};

module.exports.deleteMesecniCiljById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idCilj = zahteva.params.objectId;
  if(idCollection && idCilj){
    Cilj
      .findById(idCollection)
      .exec(
          function(napaka, cilji) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiMesecniCilj(zahteva, odgovor, cilji);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka stroskov in ciljev ne obstajata"});
  }
};

var brisiMesecniCilj = function(zahteva, odgovor, cilji){
  var idCilj = zahteva.params.objectId;
  if(idCilj){
    cilji.varcevalniCiljiMesecni.id(idCilj).remove();
    cilji.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Cilj ne obstaja"});
  }
};

module.exports.deleteObdobjeCiljById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idCilj = zahteva.params.objectId;
  if(idCollection && idCilj){
    Cilj
      .findById(idCollection)
      .exec(
          function(napaka, cilji) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiObdobjeCilj(zahteva, odgovor, cilji);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka ciljev in cilj ne obstajata"});
  }
};

var brisiObdobjeCilj = function(zahteva, odgovor, cilji){
  var idCilj = zahteva.params.objectId;
  if(idCilj){
    cilji.varcevalniCiljiObdobje.id(idCilj).remove();
    cilji.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Cilj ne obstaja"});
  }
};
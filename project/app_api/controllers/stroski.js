var mongoose = require('mongoose');
var Strosek = mongoose.model('Strosek');
var Racun = mongoose.model('Racun');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.getCollectionById = function(zahteva, odgovor) { //vraca celo zbirko
  if (zahteva.params.objectId) {
    Strosek
      .findById(zahteva.params.objectId)
      .exec(function(napaka, strosek) {
        if (!strosek) {
          vrniJsonOdgovor(odgovor, 404, { "msg":"Ne najdem stroska s podanim enoličnim identifikatorjem." });
          return;
        }
        else if(napaka){
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, strosek);
      });    
    }
    else{
      vrniJsonOdgovor(odgovor, 404, { "msg": "Manjka enolični identifikator objectId"});
    }
};

module.exports.getEnkratniStrosekById = function(zahteva, odgovor){
  //TODO - ce bo potreba, v tem primiru bi blo clo smiselno tole..
};

module.exports.getMesecniStrosekById = function(zahteva, odgovor){
  //TODO - ce bo potreba, isto kot zgoraj
};

module.exports.createNewCollection = function(zahteva, odgovor){
  if(zahteva.body.vrsta == "enkratni"){
      Strosek
      .create({
        enkratniStroski:[{
          naziv:zahteva.body.naziv,
          kolicina:zahteva.body.kolicina,
          zadnjiVnosKolicina:zahteva.body.zadnjiVnosKolicina,
          racun:zahteva.body.racun,
          odlivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
        }]
      },function(napaka, strosek){
          if (napaka){
            vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
            posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor, zahteva.body.kolicina);
            vrniJsonOdgovor(odgovor, 201, strosek);
          }
        }
      );
  }
  else if(zahteva.body.vrsta == "mesecni"){
      Strosek
      .create({
        mesecniStroski:[{
          naziv:zahteva.body.naziv,
          kolicina:zahteva.body.kolicina,
          racun:zahteva.body.racun,
          odlivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
        }]
      },function(napaka, strosek){
          if (napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
            posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor, zahteva.body.kolicina);
            vrniJsonOdgovor(odgovor, 201, strosek);
          }
        }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 400, {"msg":"Vrsta stroska ni podana."});
  }
};

module.exports.createEnkratniStrosek = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Strosek
        .findById(idCollection)
        .select('enkratniStroski')
        .exec(
          function(napaka, stroski) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajEnkratniStrosek(zahteva, odgovor, stroski);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajEnkratniStrosek = function(zahteva, odgovor, stroski){
  if (!stroski) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    stroski.enkratniStroski.push({
      naziv:zahteva.body.naziv,
      kolicina:zahteva.body.kolicina,
      zadnjiVnosKolicina:zahteva.body.zadnjiVnosKolicina,
      racun:zahteva.body.racun,
      odlivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
    });
  stroski.save(function(napaka, lokacija) {
    var dodaniStrosek;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        zahteva.kolicina = zahteva.zadnjiVnosKolicina;
        posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor,zahteva.body.zadnjiVnosKolicina);
        dodaniStrosek = stroski.enkratniStroski[stroski.enkratniStroski.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniStrosek);
      }
    });
  }
};

module.exports.createMesecniStrosek = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Strosek
        .findById(idCollection)
        .select('mesecniStroski')
        .exec(
          function(napaka, stroski) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajMesecniStrosek(zahteva, odgovor, stroski);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajMesecniStrosek = function(zahteva, odgovor, stroski){
  if (!stroski) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    stroski.mesecniStroski.push({
      naziv:zahteva.body.naziv,
      kolicina:zahteva.body.kolicina,
      racun:zahteva.body.racun,
      odlivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
    });
  stroski.save(function(napaka, lokacija) {
    var dodaniStrosek;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor,zahteva.body.kolicina);
        dodaniStrosek = stroski.mesecniStroski[stroski.mesecniStroski.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniStrosek);
      }
    });
  }
};

module.exports.createOdlivUpdateEnkratniStrosek = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  
  Strosek
    .findById(idCollection)
    .select('enkratniStroski')
    .exec(
      function(napaka, strosek) {
        var trenutniStrosek;
        if (!strosek) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (strosek.enkratniStroski && strosek.enkratniStroski.length > 0) {
          trenutniStrosek = strosek.enkratniStroski.id(zahteva.params.objectId);
          if (!trenutniStrosek) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniStrosek.kolicina = Number(trenutniStrosek.kolicina) + Number(zahteva.body.zadnjiVnosKolicina);
            trenutniStrosek.zadnjiVnosKolicina = zahteva.body.zadnjiVnosKolicina;
            trenutniStrosek.odlivi.push({
                          kolicina:zahteva.body.zadnjiVnosKolicina,
                          racun:trenutniStrosek.racun
            });
            strosek.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor,zahteva.body.zadnjiVnosKolicina);
                vrniJsonOdgovor(odgovor, 200, trenutniStrosek);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.createOdlivUpdateMesecniStrosek = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Strosek
    .findById(idCollection)
    .select('mesecniStroski')
    .exec(
      function(napaka, strosek) {
        var trenutniStrosek;
        if (!strosek) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (strosek.mesecniStroski && strosek.mesecniStroski.length > 0) {
          trenutniStrosek = strosek.mesecniStroski.id(zahteva.params.objectId);
          if (!trenutniStrosek) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniStrosek.naziv = zahteva.body.naziv;
            trenutniStrosek.kolicina = zahteva.body.kolicina;
            trenutniStrosek.racun = zahteva.body.racun;
            trenutniStrosek.odlivi.push({
                          kolicina:zahteva.body.kolicina,
                          racun:zahteva.body.racun
            });
            strosek.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor,zahteva.body.kolicina);
                vrniJsonOdgovor(odgovor, 200, trenutniStrosek);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateEnkratniStrosekById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Strosek
    .findById(idCollection)
    .select('enkratniStroski')
    .exec(
      function(napaka, strosek) {
        var trenutniStrosek;
        if (!strosek) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (strosek.enkratniStroski && strosek.enkratniStroski.length > 0) {
          trenutniStrosek = strosek.enkratniStroski.id(zahteva.params.objectId);
          if (!trenutniStrosek) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniStrosek.naziv = zahteva.body.naziv;
            trenutniStrosek.kolicina = zahteva.body.kolicina;
            trenutniStrosek.zadnjiVnosKolicina = zahteva.body.zadnjiVnosKolicina;
            trenutniStrosek.racun = zahteva.body.racun;
            strosek.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniStrosek);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateMesecniStrosekById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  
  if (!idCollection || !idStrosek) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Strosek
    .findById(idCollection)
    .select('mesecniStroski')
    .exec(
      function(napaka, strosek) {
        var trenutniStrosek;
        if (!strosek) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (strosek.mesecniStroski && strosek.mesecniStroski.length > 0) {
          trenutniStrosek = strosek.mesecniStroski.id(zahteva.params.objectId);
          if (!trenutniStrosek) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniStrosek.naziv = zahteva.body.naziv;
            trenutniStrosek.kolicina = zahteva.body.kolicina;
            trenutniStrosek.racun = zahteva.body.racun;
            strosek.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniStrosek);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.deleteById =  function(zahteva, odgovor) {
  var idStroska = zahteva.params.objectId; //tole je v resnici cela zbirka
    if (idStroska) {
      Strosek
        .findByIdAndRemove(idStroska)
        .exec(function(napaka, strosek) {
          if (napaka) {
              vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        });
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka stroskov ni podana"});
    }
};

module.exports.deleteEnkratniStrosekById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  if(idCollection && idStrosek){
    Strosek
      .findById(idCollection)
      .exec(
          function(napaka, stroski) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiEnkratniStrosek(zahteva, odgovor, stroski);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka stroskov in strosek ne obstajata"});
  }
};

var brisiEnkratniStrosek = function(zahteva, odgovor, stroski){
  var idStrosek = zahteva.params.objectId;
  if(idStrosek){
    stroski.enkratniStroski.id(idStrosek).remove();
    stroski.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Strosek ne obstaja"});
  }
};

module.exports.deleteMesecniStrosekById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idStrosek = zahteva.params.objectId;
  if(idCollection && idStrosek){
    Strosek
      .findById(idCollection)
      .exec(
          function(napaka, stroski) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiMesecniStrosek(zahteva, odgovor, stroski);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka stroskov in strosek ne obstajata"});
  }
};

var brisiMesecniStrosek = function(zahteva, odgovor, stroski){
  var idStrosek = zahteva.params.objectId;
  if(idStrosek){
    stroski.mesecniStroski.id(idStrosek).remove();
    stroski.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Strosek ne obstaja"});
  }
};

var posodobiRacun = function(kolekcijaId, racunId, zahteva, odgovor, kolicina){
  Racun
    .findById(kolekcijaId)
    .select('racuni')
    .exec(
      function(napaka, racun) {
        var trenutniRacun;
        if (!racun) {
          //vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija racunov ne obstaja."});
          console.log("err1");
          return;
        }
        else if (napaka) {
          //vrniJsonOdgovor(odgovor, 400, napaka);
          console.log("err2");
          return;
        }
        if (racun.racuni && racun.racuni.length > 0) {
          trenutniRacun = racun.racuni.id(zahteva.body.racun);
          if (!trenutniRacun) {
            //vrniJsonOdgovor(odgovor, 404, {"msg": "Nema računa."});
            console.log("err3");
          }
          else {
            trenutniRacun.stanje = Number(trenutniRacun.stanje) - Number(kolicina);
            trenutniRacun.odlivi.push({
              kolicina:kolicina
            });
            racun.save(function(napaka, lokacija) {
              if (napaka) {
                //vrniJsonOdgovor(odgovor, 404, napaka);
                console.log("err4" + napaka);
              }
              else{
                //vrniJsonOdgovor(odgovor, 200, trenutniRacun);
                console.log("ok");
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};
var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');
var xmlify = require('xmlify');


var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

var vrniXMLOdgovor = function(odgovor, status, vsebina, root){
    odgovor.status(status);
    odgovor.set('Content-Type', 'text/xml');
    odgovor.send(xmlify(vsebina, root));
};

var vrniPlainTextOdgovor = function(odgovor, status, vsebina){
    odgovor.status(status);
    odgovor.set('Content-Type', 'text/plain');
    var str = JSON.stringify(vsebina);
    str = str.replace(/[\[\]\{\}]+/g, '');
    odgovor.send(str);
};

//GET

module.exports.getAll = function(zahteva, odgovor) {
  Uporabnik
    .find({})
    .exec(function(napaka, uporabnik) {
        if(zahteva.get('Accept') == 'text/xml'){
            vrniXMLOdgovor(odgovor, 200, uporabnik, 'uporabnikis');
        }
        else if(zahteva.get('Accept') == 'text/plain'){
            vrniPlainTextOdgovor(odgovor, 200, uporabnik);
        }
        else{
            vrniJsonOdgovor(odgovor, 200, uporabnik);
        }
    });
};

module.exports.getById = function(zahteva, odgovor){
  Uporabnik
    .findById(zahteva.params.objectId)
    .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
            vrniJsonOdgovor(odgovor, 404, { "msg":"Uporabnik ne obstaja." });
            return;
        }
        else if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if(zahteva.get('Accept') == 'text/xml'){
            vrniXMLOdgovor(odgovor, 200, uporabnik, 'uporabnik');
        }
        else if(zahteva.get('Accept') == 'text/plain'){
            vrniPlainTextOdgovor(odgovor, 200, uporabnik);
        }
        else{
            vrniJsonOdgovor(odgovor, 200, uporabnik);
        }
    });  
};

module.exports.getByEmail = function(zahteva, odgovor) {
  Uporabnik
    .findOne({email:zahteva.params.email})
    .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
            vrniJsonOdgovor(odgovor, 404, { "msg":"Uporabnik ne obstaja." });
            return;
        }
        else if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if(zahteva.get('Accept') == 'text/xml'){
            vrniXMLOdgovor(odgovor, 200, uporabnik, 'uporabnik');
        }
        else if(zahteva.get('Accept') == 'text/plain'){
            vrniPlainTextOdgovor(odgovor, 200, uporabnik);
        }
        else{
            vrniJsonOdgovor(odgovor, 200, uporabnik);
        }
    });
};

module.exports.getByFacebookId = function(zahteva, odgovor) {
  Uporabnik
    .find({facebookId:zahteva.params.facebookId})
    .exec(function(napaka, uporabnik) {
        if (!uporabnik) {
            vrniJsonOdgovor(odgovor, 404, { "msg":"Uporabnik ne obstaja." });
            return;
        }
        else if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if(zahteva.get('Accept') == 'text/xml'){
            vrniXMLOdgovor(odgovor, 200, uporabnik, 'uporabniki');
        }
        else if(zahteva.get('Accept') == 'text/plain'){
            vrniPlainTextOdgovor(odgovor, 200, uporabnik);
        }
        else{
            vrniJsonOdgovor(odgovor, 200, uporabnik);
        }
    });
};

//POST
//ob registraciji, kar je null se bo kreiralo ko bo prislo do potrebe po tem
module.exports.createNew = function(zahteva, odgovor) {
    var uporabnik = new Uporabnik();
    uporabnik.vloge = zahteva.body.vloge
    uporabnik.ime = zahteva.body.ime
    uporabnik.priimek = zahteva.body.priimek
    uporabnik.email = zahteva.body.email
    
    if(zahteva.body.geslo != null){
        uporabnik.nastaviGeslo(zahteva.body.geslo);
    }
    
    uporabnik.facebookId = zahteva.body.facebookId,
    uporabnik.racuni = null
    uporabnik.prilivi = null
    uporabnik.stroski = null
    uporabnik.cilji = null
    
    uporabnik.save(function(napaka) {
        var zeton;
            if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
            }
            else{
                zeton = uporabnik.generirajJwt();
                vrniJsonOdgovor(odgovor, 200, {"zeton": zeton, "uporabnik":uporabnik});
            }
    });

};

//PUT

module.exports.updateExisting = function(zahteva,odgovor){
    if (!zahteva.params.objectId) {
        vrniJsonOdgovor(odgovor, 404, {"msg": "Uporabnik ne obstaja."});
        return;
    }
    Uporabnik
        .findById(zahteva.params.objectId)
        .exec(function(napaka, uporabnik) {
            if (!uporabnik) {
                vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem uporabnika."});
            return;
        }
        else if(napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
        return;
        }
            uporabnik.vloge = zahteva.body.vloge,
            uporabnik.ime = zahteva.body.ime,
            uporabnik.priimek = zahteva.body.priimek,
            uporabnik.email = zahteva.body.email,
            uporabnik.geslo = zahteva.body.geslo,
            uporabnik.facebookId = zahteva.body.facebookId,
            uporabnik.racuni = zahteva.body.racuni,
            uporabnik.prilivi = zahteva.body.prilivi,
            uporabnik.stroski = zahteva.body.stroski,
            uporabnik.cilji = zahteva.body.cilji;
            uporabnik.save(function(napaka, uporabnik) {
                            if (napaka)
                                vrniJsonOdgovor(odgovor, 404, napaka);
                            else
                            vrniJsonOdgovor(odgovor, 200, uporabnik);
                            });
        });
};

//DELETE
//ne zbriše uporabnikovih podatkov, samo uporabnika, če bo cajt implementiraj tko k se zagre, da nou baza cis posvinana
module.exports.deleteById = function(zahteva, odgovor) {
    var idUporabnika = zahteva.params.objectId;
        if (idUporabnika) {
            Uporabnik
                .findByIdAndRemove(idUporabnika)
                .exec(function(napaka, uporabnik) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 404, napaka);
                    return;
                    }
                vrniJsonOdgovor(odgovor, 204, null);
                });
        }
        else{
            vrniJsonOdgovor(odgovor, 404, {"msg": "Uporabnik ne obstaja"});
        }
};
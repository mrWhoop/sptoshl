var mongoose = require('mongoose');
var Priliv = mongoose.model('Priliv');
var Racun = mongoose.model('Racun');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.getCollectionById = function(zahteva, odgovor) { //vraca celo zbirko prilivov
  if (zahteva.params.objectId) {
    Priliv
      .findById(zahteva.params.objectId)
      .exec(function(napaka, priliv) {
        if (!priliv) {
          vrniJsonOdgovor(odgovor, 404, { "msg":"Ne najdem priliva s podanim enoličnim identifikatorjem." });
          return;
        }
        else if(napaka){
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, priliv);
      });    
    }
    else{
      vrniJsonOdgovor(odgovor, 404, { "msg": "Manjka enolični identifikator objectId"});
    }
};

module.exports.getEnkratniPrilivById = function(zahteva, odgovor){
  //TODO - ce bo potreba
};

module.exports.getMesecniPrilivById = function(zahteva, odgovor){
  //TODO - ce bo potreba
};

module.exports.createNewCollection = function(zahteva, odgovor){
  if(zahteva.body.vrsta == "enkratni"){
      Priliv
      .create({
        enkratniPrilivi:[{
          naziv:zahteva.body.naziv,
          kolicina:zahteva.body.kolicina,
          racun:zahteva.body.racun
        }]
      },function(napaka, priliv){
          if (napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
              posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor);
              vrniJsonOdgovor(odgovor, 201, priliv);
          }
        }
      );
  }
  else if(zahteva.body.vrsta == "mesecni"){
      Priliv
      .create({
        mesecniPrilivi:[{
          naziv:zahteva.body.naziv,
          kolicina:zahteva.body.kolicina,
          racun:zahteva.body.racun,
          prilivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
        }]
      },function(napaka, racun){
          if (napaka){
                vrniJsonOdgovor(odgovor, 400, napaka);
          }
          else{
              posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor);
              vrniJsonOdgovor(odgovor, 201, racun);
          }
        }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 400, {"msg":"Vrsta racuna ni podana."});
  }
};

module.exports.createEnkratniPriliv = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Priliv
        .findById(idCollection)
        .select('enkratniPrilivi')
        .exec(
          function(napaka, prilivi) {
            if (napaka){
              vrniJsonOdgovor(odgovor, 400, napaka);
            }
            else{
               
              dodajEnkratniPriliv(zahteva, odgovor, prilivi);
            }
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajEnkratniPriliv = function(zahteva, odgovor, prilivi){
  if (!prilivi) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    prilivi.enkratniPrilivi.push({
      naziv:zahteva.body.naziv,
      kolicina:zahteva.body.kolicina,
      racun:zahteva.body.racun
    });
  prilivi.save(function(napaka, lokacija) {
    var dodaniPriliv;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor);
        dodaniPriliv = prilivi.enkratniPrilivi[prilivi.enkratniPrilivi.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniPriliv);
      }
    });
  }
};

module.exports.createMesecniPriliv = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Priliv
        .findById(idCollection)
        .select('mesecniPrilivi')
        .exec(
          function(napaka, prilivi) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajMesecniPriliv(zahteva, odgovor, prilivi);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajMesecniPriliv = function(zahteva, odgovor, prilivi){
  if (!prilivi) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    prilivi.mesecniPrilivi.push({
      naziv:zahteva.body.naziv,
      kolicina:zahteva.body.kolicina,
      racun:zahteva.body.racun,
      prilivi:[{
            kolicina:zahteva.body.kolicina,
            racun:zahteva.body.racun
          }]
    });
  prilivi.save(function(napaka, lokacija) {
    var dodaniPriliv;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor);
        dodaniPriliv = prilivi.mesecniPrilivi[prilivi.mesecniPrilivi.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniPriliv);
      }
    });
  }
};

module.exports.createPrilivUpdateMesecniPriliv = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idPriliv = zahteva.params.objectId;
  
  if (!idCollection || !idPriliv) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Priliv
    .findById(idCollection)
    .select('mesecniPrilivi')
    .exec(
      function(napaka, priliv) {
        var trenutniPriliv;
        if (!priliv) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija stroskov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (priliv.mesecniPrilivi && priliv.mesecniPrilivi.length > 0) {
          trenutniPriliv = priliv.mesecniPrilivi.id(zahteva.params.objectId);
          if (!priliv.mesecniPrilivi) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stroska."});
          }
          else {
            trenutniPriliv.naziv = zahteva.body.naziv;
            trenutniPriliv.kolicina = zahteva.body.kolicina;
            trenutniPriliv.racun = zahteva.body.racun;
            trenutniPriliv.prilivi.push({
                          kolicina:zahteva.body.kolicina,
                          racun:zahteva.body.racun
            });
            priliv.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                posodobiRacun(zahteva.body.racunCollectionId, zahteva.body.racun, zahteva, odgovor);
                vrniJsonOdgovor(odgovor, 200, trenutniPriliv);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateEnkratniPrilivById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idPriliv = zahteva.params.objectId;
  
  if (!idCollection || !idPriliv) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Priliv
    .findById(idCollection)
    .select('enkratniPrilivi')
    .exec(
      function(napaka, priliv) {
        var trenutniPriliv;
        if (!priliv) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija prilivov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (priliv.enkratniPrilivi && priliv.enkratniPrilivi.length > 0) {
          trenutniPriliv = priliv.enkratniPrilivi.id(zahteva.params.objectId);
          if (!trenutniPriliv) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema priliva."});
          }
          else {
            trenutniPriliv.naziv = zahteva.body.naziv;
            trenutniPriliv.kolicina = zahteva.body.kolicina;
            trenutniPriliv.racun = zahteva.body.racun;
            priliv.save(function(napaka, lokacija) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniPriliv);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.updateMesecniPrilivById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idPriliv = zahteva.params.objectId;
  
  if (!idCollection || !idPriliv) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Priliv
    .findById(idCollection)
    .select('mesecniPrilivi')
    .exec(
      function(napaka, priliv) {
        var trenutniPriliv;
        if (!priliv) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija prilivov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (priliv.mesecniPrilivi && priliv.mesecniPrilivi.length > 0) {
          trenutniPriliv = priliv.mesecniPrilivi.id(zahteva.params.objectId);
          if (!trenutniPriliv) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema priliva."});
          }
          else {
            trenutniPriliv.naziv = zahteva.body.naziv;
            trenutniPriliv.kolicina = zahteva.body.kolicina;
            trenutniPriliv.racun = zahteva.body.racun;
            priliv.save(function(napaka, trenutniPriliv) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniPriliv);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

module.exports.deleteById =  function(zahteva, odgovor){
  var idPriliva = zahteva.params.objectId; //tole je v resnici cela zbirka
    if (idPriliva) {
        Priliv
            .findByIdAndRemove(idPriliva)
            .exec(function(napaka, priliv) {
                if (napaka) {
                    vrniJsonOdgovor(odgovor, 404, napaka);
                return;
                }
            vrniJsonOdgovor(odgovor, 204, null);
            });
    }
    else{
        vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka racunov ne obstaja"});
    }
};

module.exports.deleteEnkratniPrilivById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idPriliv = zahteva.params.objectId;
  if(idCollection && idPriliv){
    Priliv
      .findById(idCollection)
      .exec(
          function(napaka, prilivi) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiEnkratniPriliv(zahteva, odgovor, prilivi);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka racunov in racun ne obstajata"});
  }
};

var brisiEnkratniPriliv = function(zahteva, odgovor, prilivi){
  var idPriliv = zahteva.params.objectId;
  if(idPriliv){
    prilivi.enkratniPrilivi.id(idPriliv).remove();
    prilivi.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Priliv ne obstaja"});
  }
};

module.exports.deleteMesecniPrilivById = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idPriliv = zahteva.params.objectId;
  if(idCollection && idPriliv){
    Priliv
      .findById(idCollection)
      .exec(
          function(napaka, prilivi) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiMesecniPriliv(zahteva, odgovor, prilivi);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka racunov in racun ne obstajata"});
  }
};

var brisiMesecniPriliv = function(zahteva, odgovor, prilivi){
  var idPriliv = zahteva.params.objectId;
  if(idPriliv){
    prilivi.mesecniPrilivi.id(idPriliv).remove();
    prilivi.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Priliv ne obstaja"});
  }
};

var posodobiRacun = function(kolekcijaId, racunId, zahteva, odgovor){
  Racun
    .findById(kolekcijaId)
    .select('racuni')
    .exec(
      function(napaka, racun) {
        var trenutniRacun;
        if (!racun) {
          //vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija racunov ne obstaja."});
          console.log("err1")
          return;
        }
        else if (napaka) {
          //vrniJsonOdgovor(odgovor, 400, napaka);
          console.log("err2")
          return;
        }
        if (racun.racuni && racun.racuni.length > 0) {
          trenutniRacun = racun.racuni.id(zahteva.body.racun);
          if (!trenutniRacun) {
            //vrniJsonOdgovor(odgovor, 404, {"msg": "Nema računa."});
            console.log("err3")
          }
          else {
            trenutniRacun.stanje = Number(trenutniRacun.stanje) + Number(zahteva.body.kolicina);
            trenutniRacun.prilivi.push({
              kolicina:zahteva.body.kolicina
            });
            racun.save(function(napaka, lokacija) {
              if (napaka) {
                //vrniJsonOdgovor(odgovor, 404, napaka);
                console.log("err4" + napaka)
              }
              else{
                //vrniJsonOdgovor(odgovor, 200, trenutniRacun);
                console.log("ok")
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};
var mongoose = require('mongoose');
var Racun = mongoose.model('Racun');
var xmlify = require('xmlify');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

var vrniXMLOdgovor = function(odgovor, status, vsebina, root){
    odgovor.status(status);
    odgovor.set('Content-Type', 'text/xml');
    odgovor.send(xmlify(vsebina, root));
};

var vrniPlainTextOdgovor = function(odgovor, status, vsebina){
    odgovor.status(status);
    odgovor.set('Content-Type', 'text/plain');
    var str = JSON.stringify(vsebina);
    str = str.replace(/[\[\]\{\}]+/g, '');
    odgovor.send(str);
};

//GET
module.exports.getCollectionById = function(zahteva, odgovor) { //vraca celo zbirko racunov
  if (zahteva.params.objectId) {
    Racun
      .findById(zahteva.params.objectId)
      .exec(function(napaka, racun) {
        if (!racun) {
          vrniJsonOdgovor(odgovor, 404, { "msg":"Ne najdem računa s podanim enoličnim identifikatorjem." });
          return;
        }
        else if(napaka){
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        if(zahteva.get('Accept') == 'text/xml'){
            vrniXMLOdgovor(odgovor, 200, racun, 'racunis');
        }
        else if(zahteva.get('Accept') == 'text/plain'){
            vrniPlainTextOdgovor(odgovor, 200, racun);
        }
        else{
            vrniJsonOdgovor(odgovor, 200, racun);
        }
      });    
    }
    else{
      vrniJsonOdgovor(odgovor, 404, { "msg": "Manjka enolični identifikator objectId"});
    }
};

module.exports.getById = function (zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Racun
        .findById(idCollection)
        .select('racuni')
        .exec(
          function(napaka, racuni) {
            if (napaka){
              vrniJsonOdgovor(odgovor, 400, napaka);
            }
            else{
              if (racuni.enkratniStroski && racuni.enkratniStroski.length > 0) {
                var trenutniRacun = racuni.racuni.id(zahteva.params.objectId);
                if (!trenutniRacun) {
                  vrniJsonOdgovor(odgovor, 404, {"msg": "Nema racuna."});
                }
                else {
                 if(zahteva.get('Accept') == 'text/xml'){
                      vrniXMLOdgovor(odgovor, 200, trenutniRacun, 'racunis');
                  }
                  else if(zahteva.get('Accept') == 'text/plain'){
                      vrniPlainTextOdgovor(odgovor, 200, trenutniRacun);
                  }
                  else{
                      vrniJsonOdgovor(odgovor, 200, trenutniRacun);
                  }
                }
              }
            }
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

//POST
//ob kreiranju kolekcije ie. dodajanju prvega racuna
module.exports.createNewCollection = function(zahteva, odgovor){
    Racun
      .create({
        racuni:[{
          naziv:zahteva.body.naziv,
          stanje:zahteva.body.stanje,
          odlivi:[],
          prilivi:[{
            kolicina:zahteva.body.stanje
          }]
        }]
      },function(napaka, racun){
          if (napaka)
                vrniJsonOdgovor(odgovor, 400, napaka);
          else
              vrniJsonOdgovor(odgovor, 201, racun);
        }
      );
};
//ob dodajanju novega racuna v kolekcijo
module.exports.createNewRacun = function(zahteva, odgovor){
    var idCollection = zahteva.params.collectionId;
    if(idCollection) {
      Racun
        .findById(idCollection)
        .select('racuni')
        .exec(
          function(napaka, racuni) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              dodajRacun(zahteva, odgovor, racuni);
          }
      );
    }
    else{
      vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
    }
};

var dodajRacun = function(zahteva, odgovor, racuni){
  if (!racuni) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Ne najdem kolekcije."});
  }
  else{
    racuni.racuni.push({
      naziv:zahteva.body.naziv,
      stanje:zahteva.body.stanje,
      odlivi:[],
      prilivi:[{
        kolicina:zahteva.body.stanje
      }]
    });
  racuni.save(function(napaka, lokacija) {
    var dodaniRacun;
      if (napaka) {
        vrniJsonOdgovor(odgovor, 400, napaka);
      }
      else{
        dodaniRacun = racuni.racuni[racuni.racuni.length - 1];
        vrniJsonOdgovor(odgovor, 201, dodaniRacun);
      }
    });
  }
};

//PUT
//posodobitve računa
//stvari kot so naziv, stanja ne posodabljamo razen skozi prilive in odlive
//naziv: posodobi ime računa
//vse zgornje je definirano v poslani zahtevi

module.exports.updateRacun = function(zahteva, odgovor){
  var idCollection = zahteva.params.collectionId;
  var idRacuna = zahteva.params.objectId;
  
  if (!idCollection || !idRacuna) {
    vrniJsonOdgovor(odgovor, 404, {"msg": "Napacen klic ze s prve."});
    return;
  }
  Racun
    .findById(idCollection)
    .select('racuni')
    .exec(
      function(napaka, racun) {
        var trenutniRacun;
        if (!racun) {
          vrniJsonOdgovor(odgovor, 404, {"msg": "Kolekcija racunov ne obstaja."});
          return;
        }
        else if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
          return;
        }
        if (racun.racuni && racun.racuni.length > 0) {
          trenutniRacun = racun.racuni.id(zahteva.params.objectId);
          if (!trenutniRacun) {
            vrniJsonOdgovor(odgovor, 404, {"msg": "Nema računa."});
          }
          else {
            trenutniRacun.naziv = zahteva.body.naziv;
            trenutniRacun.stanje = zahteva.body.stanje;
            racun.save(function(napaka, trenutniRacun) {
              if (napaka) {
                vrniJsonOdgovor(odgovor, 404, napaka);
              }
              else{
                vrniJsonOdgovor(odgovor, 200, trenutniRacun);
              }
            });
          }
        }
        else{
          vrniJsonOdgovor(odgovor, 404, {"msg": "Nema stari."});
        }
      });
};

//DELETE
module.exports.deleteCollectionById =  function(zahteva, odgovor) { //use carefully (samo ob brisanju uporabnika)
  var idRacuna = zahteva.params.objectId; //tole je v resnici cela zbirka
        if (idRacuna) {
            Racun
                .findByIdAndRemove(idRacuna)
                .exec(function(napaka, racun) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 404, napaka);
                    return;
                    }
                vrniJsonOdgovor(odgovor, 204, null);
                });
        }
        else{
            vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka racunov ne obstaja"});
        }
};

module.exports.deleteRacun = function(zahteva, odgovor){ //brsanje enega racuna
  var idCollection = zahteva.params.collectionId;
  var idRacun = zahteva.params.objectId;
  if(idCollection && idRacun){
    Racun
      .findById(idCollection)
      .exec(
          function(napaka, racuni) {
            if (napaka)
              vrniJsonOdgovor(odgovor, 400, napaka);
            else
              brisiRacun(zahteva, odgovor, racuni);
          }
      );
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Zbirka racunov in racun ne obstajata"});
  }
};

var brisiRacun = function(zahteva, odgovor, racuni){
  var idRacun = zahteva.params.objectId;
  if(idRacun){
    racuni.racuni.id(idRacun).remove();
    racuni.save(function(napaka) {
      if (napaka) {
       vrniJsonOdgovor(odgovor, 404, napaka);
      }
      else{
       vrniJsonOdgovor(odgovor,  204, null);
      }
    });
  }
  else{
    vrniJsonOdgovor(odgovor, 404, {"msg": "Racun ne obstaja"});
  }
};
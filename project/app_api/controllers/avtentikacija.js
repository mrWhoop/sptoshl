var passport = require('passport');
var mongoose = require('mongoose');
var Uporabniki = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.prijava = function(zahteva, odgovor) {
    if (!zahteva.body.email || !zahteva.body.geslo) {
        vrniJsonOdgovor(odgovor, 400, {
        "msg": "Zahtevani so vsi podatki"
        });
    }

    passport.authenticate('local', function(napaka, uporabnik, podatki) {
    var zeton;
        if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if (uporabnik) {
            zeton = uporabnik.generirajJwt();
            vrniJsonOdgovor(odgovor, 200, {"zeton": zeton , "uporabnik":uporabnik});
        }
        else {
            vrniJsonOdgovor(odgovor, 401, podatki);
        }
    })(zahteva, odgovor);
};

module.exports.facebook = function(zahteva, odgovor){
    passport.authenticate('facebook', function(napaka, uporabnik, podatki){
        var zeton;
        if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
        }
        if (uporabnik) {
            zeton = uporabnik.generirajJwt();
            vrniJsonOdgovor(odgovor, 200, {"zeton": zeton , "uporabnik":uporabnik});
        }
        else {
            vrniJsonOdgovor(odgovor, 401, podatki);
        }
    })(zahteva, odgovor);
}


